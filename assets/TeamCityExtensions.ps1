Function TC-Message
{
    param(
        [string] $message
        )
    write-host "##teamcity[message text='$message']"
}