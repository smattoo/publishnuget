param(
	[string]$slnPath=".\src",
    [string]$projPath = "",
    [string]$apikey="",
    [string]$nugetFolder,
    [string]$branch
)

$scriptFolder=$myInvocation.MyCommand.Path | Split-Path -parent
. "$scriptFolder\TeamCityExtensions.ps1"

Function InValidBranch{
	$branchLower = $branch.ToLower()
	if($branchLower.CompareTo("master") -eq 0 -or $branchLower.CompareTo("prod") -eq 0) {
	 	return  $False	
	}	
	return  $True
}

if(InValidBranch)
{
	TC-Message "Skipping publishing to nuget.org as build is not from master/prod branch."
	exit
}

TC-Message "Starting publishing to nuget.org for $projPath"

Function Publish {
	$nugetFolder =  gci -Path $projPath -Filter $nugetFolder -recurse
	gci -Path $nugetFolder.FullName -Filter "*.nupkg" | 
		Where-Object { $_.FullName -NotMatch ".symbols."  }   |
		ForEach-Object {
			$nugetPackageFilePath = $_.FullName
			TC-Message "Publishing $nugetPackageFilePath to nuget.org"
			$params="push", "$nugetPackageFilePath","-ApiKey", "$apikey","-Source","https://www.nuget.org"
			TC-Message $params
			(& $nuget $params)	
		}
}

Function PackageVersionExists {
   return $existingPackages.Contains($($packageId + " " + $versionId))
}

Function PackageNotFound {
	return $existingPackages -eq "No packages found."
}

$nuget=(ls "$slnPath\.nuget\Nuget.exe")

$path = "$projPath"

$versionFile = gci -Path $path -Filter "version.txt"
$versionId = cat($versionFile.FullName)

$packageId = $versionFile.Directory.Name

$params="list", $packageId, "-Source", "https://www.nuget.org/api/v2/"
$existingPackages = (& $nuget $params)

if(PackageNotFound) {
	TC-Message "Package $packageId $versionId not found in nuget.org"
	Publish
}
else{	 
	if(PackageVersionExists) {
		TC-Message "Skipped publishing to nuget.org as package $packageId with version $versionId already exists."
	}
	else{
		TC-Message "Package $packageId $versionId not found in nuget.org"
		Publish
	}
}