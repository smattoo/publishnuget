using System;
using System.Security;
using System.Text;
using System.Text.RegularExpressions;

namespace Brewmaster.TemplateSDK.Contracts
{
    public static class StringExtensions
    {
        private static readonly Random Randomizer = new Random();

        public static SecureString ToSecure(this string current)
        {
            var secure = new SecureString();
            foreach (var c in current.ToCharArray()) secure.AppendChar(c);
            return secure;
        }

        public static string WildcardToRegex(this string pattern)
        {
            return "^" + Regex.Escape(pattern).
                               Replace(@"\*", ".*").
                               Replace(@"\?", ".") + "$";
        }

        public static bool IsWildcard(this string s)
        {
            if (s == null) return false;
            return s.IndexOfAny(new[] { '*', '?' }) != -1;
        }

        public static int CompareNoCase(this string current, string other)
        {
            return String.Compare(current, other, StringComparison.OrdinalIgnoreCase);
        }

        public static bool EqualNoCase(this string current, string other)
        {
            return String.Equals(current, other, StringComparison.OrdinalIgnoreCase);
        }

        public static string RandomString(int len, bool noLower = false, bool noUpper = false,
                                          bool noNumbers = false, bool noSymbols = false)
        {
            var charset = "";
            if (!noLower) charset += "abcdefghijklmnopqrstuvwxyz";
            if (!noUpper) charset += "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            if (!noNumbers) charset += "0123456789";
            if (!noSymbols) charset += @"!""#$%&'()*+,-./:;<=>?@[\]^_`{|}~";
            return RandomString(len, charset);
        }

        public static string RandomString(int len, string charset)
        {
            if (len < 0) throw new ArgumentOutOfRangeException("len");
            if (String.IsNullOrEmpty(charset)) throw new ArgumentNullException("charset");
            var rg = new byte[len];
            Randomizer.NextBytes(rg);
            var sb = new StringBuilder(len);
            for (var i = 0; i < len; i++)
                sb.Append(charset[rg[i] % charset.Length]);
            return sb.ToString();
        }

        public static string RandomAlphanumString(int len, bool lowercase = false)
        {
            return RandomString(len, noUpper: lowercase, noSymbols: true);
        }

        public static string RandomAlphaString(int len, bool lowercase = false)
        {
            return RandomString(len, noUpper: lowercase, noNumbers: true, noSymbols: true);
        }
    }
}