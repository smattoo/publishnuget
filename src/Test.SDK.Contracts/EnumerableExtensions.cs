using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;

namespace Brewmaster.TemplateSDK.Contracts
{
    public static class EnumerableExtensions
    {
        [NotNull]
        public static IEnumerable<T> Safe<T>([CanBeNull] this IEnumerable<T> source)
        {
            return source ?? Enumerable.Empty<T>();
        }

        [NotNull]
        public static T[] Safe<T>([CanBeNull] this T[] source)
        {
            return source ?? new T[0];
        }

        [NotNull]
        public static HashSet<T> ToHashSet<T>([CanBeNull] this IEnumerable<T> source, IEqualityComparer<T> comparer = null)
        {
            return new HashSet<T>(source.Safe(), comparer ?? EqualityComparer<T>.Default);
        }

        public static void ForEach<T>([CanBeNull] this IEnumerable<T> source, Action<T> action)
        {
            foreach (var item in source.Safe())
            {
                action(item);
            }
        }
    }
}
