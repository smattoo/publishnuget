using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;

namespace Brewmaster.TemplateSDK.Contracts.Validation
{
    public static class ValidationHelpers
    {
        //TODO: Move these constants to external config
        public const int MinAzureDataSizeSizeInGB = 1;
        public const int MaxAzureDataSizeSizeInGB = 1024;
        public const int MinAzureDataDiskLun = 0;
        public const int MaxAzureDataDiskLun = 15;
        public static readonly string[] AzureRoleSizes = new[]
            {
                "ExtraSmall",
                "Small",
                "Medium",
                "Large",
                "ExtraLarge",
                "Basic_A0",
                "Basic_A1",
                "Basic_A2",
                "Basic_A3",
                "Basic_A4",
                "A5",
                "A6",
                "A7",
                "A8",
                "A9",
            };

        public static readonly string[] AzureRegions = new[]
            {
                "East US",
                "West US",
                "North Europe",
                "West Europe",
                "East Asia",
                "Southeast Asia",
                "Japan East",
                "Japan West",
            };

        public static readonly string[] AzureDataDiskHostCachingOptions =
            new[] {"None", "ReadWrite", "ReadOnly"};
        public static readonly string[] AzureEndpointProtocolOptions =
            new[] { "TCP", "UDP" };
        public static readonly string[] AzureEndpointProbeProtocolOptions =
            new[] { "HTTP", "TCP" };
        public const int AzureEndpointProbeMinIntervalInSeconds = 5;
        public const int AzureEndpointProbeMinTimeoutInSeconds = 11;

        public static readonly string[] AzureEndpointACLActions =
            new[] {"permit", "deny"};

        public const int VPNClientMaxAddresses = 256;


        public static void ValidateIdentifier(string identifier, string paramName = null)
        {
            if (identifier.Trim() != identifier)
                throw new ArgumentException(
                    "Invalid identifier. Must not start or end with whitespace.",
                    paramName);
        }

        public static void ValidateParameterName(string name, string paramName = null)
        {
            if (!Regex.IsMatch(name, @"^[a-zA-Z0-9_]{1,256}$", RegexOptions.CultureInvariant))
                throw new ArgumentException(
                    "Invalid Parameter name. Must contain 1 to 256 letters, numbers, or underscores",
                    paramName);
        }

        public static void ValidateAffinityGroupName(string name, string paramName = null)
        {
            if (!Regex.IsMatch(name, @"^[a-zA-Z0-9][a-zA-Z0-9-]{0,62}$", RegexOptions.CultureInvariant))
                throw new ArgumentException(
                    "Invalid Affinity Group name. Must contain 1 to 63 letters, numbers, and hyphens. Must start with a letter or number",
                    paramName);
        }

        public static void ValidateRegionName(string name, string paramName = null)
        {
            if (String.IsNullOrEmpty(name) || !AzureRegions.Contains(name, StringComparer.OrdinalIgnoreCase))
                throw new ArgumentException(
                    String.Format(CultureInfo.InvariantCulture, "Invalid Region name. Must be {0}",
                                  ConjoinWords(AzureRegions, "or")), paramName);
        }

        public static void ValidateStorageAccountName(string name, string paramName = null)
        {
            if (!Regex.IsMatch(name, @"^[0-9a-zA-Z]{3,24}$", RegexOptions.CultureInvariant))
                throw new ArgumentException(
                    "Invalid Storage Account name. Must contain 3 to 24 letters and numbers.", paramName);
        }

        public static void ValidateCloudServiceName(string name, string paramName = null)
        {
            if (!Regex.IsMatch(name, @"^[0-9a-zA-Z][0-9a-zA-Z-]{0,61}[0-9a-zA-Z]$", RegexOptions.CultureInvariant))
                throw new ArgumentException(
                    "Invalid Cloud Service name. Must contain 2 to 63 letters, numbers, and hyphens. Must start and end with a letter or number.",
                    paramName);
        }

        public static void ValidateDeploymentName(string name, string paramName = null)
        {
            if (!Regex.IsMatch(name, @"^[a-zA-Z0-9_][a-zA-Z0-9_-]{0,62}$", RegexOptions.CultureInvariant))
                throw new ArgumentException(
                    "Invalid Deployment name. Must contain 1 to 63 letters, numbers, hyphens, and underscores. Must start with a letter, number, or underscore.",
                    paramName);
        }

        public static void ValidateVirtualMachineName(string name, string paramName = null)
        {
            if (!Regex.IsMatch(name, @"^[a-zA-Z][a-zA-Z0-9-]{1,13}[a-zA-Z0-9]$", RegexOptions.CultureInvariant))
                throw new ArgumentException(
                    "Invalid Virtual Machine name. Must contain 3 to 15 letters, numbers, and hyphens. Must start with a letter and end with a letter or a number.",
                    paramName);
        }

        public static void ValidateDomainName(string fullyQualifedDomainName, string paramName = null)
        {
            if (!Regex.IsMatch(fullyQualifedDomainName,
                               @"[a-z]([a-z0-9-]*[a-z0-9])?(\.[a-z]([a-z0-9-]*[a-z0-9])?)*",
                               RegexOptions.IgnoreCase | RegexOptions.CultureInvariant))
                throw new ArgumentException(
                    "Invalid domain name. Must consist of one or more dot-separated names starting with a letter and contain only letters, numbers, and hyphens.",
                    paramName);
        }

        public static void ValidateWindowsComputerName(string name, string paramName = null)
        {
            if (!Regex.IsMatch(name, @"^[^ .""\/\\\[\]<>:|+=;?*][^.""\/\\\[\]<>:|+=;?*]{0,14}$",
                               RegexOptions.CultureInvariant))
                throw new ArgumentException(
                    @"Invalid Windows computer name. Must contain 1 to 15 characters. Cannot start with space and cannot contain the following characters: "" . / \ [ ] < > : | + = ; ? *",
                    paramName);

            if (Regex.IsMatch(name, @"^\d+$", RegexOptions.CultureInvariant))
                throw new ArgumentException(
                    @"Invalid Windows computer name. Cannot be all digits.",
                    paramName);
        }

        public static void ValidateWindowsUserName(string name, string paramName = null)
        {
            if (name.Contains('\\'))
            {
                //domain_or_computer\joe
                var parts = name.Split(new[] {'\\'}, 2);
                ValidateDomainName(parts[0], paramName);
                name = parts[1];
            }
            else if (name.Contains('@'))
            {
                //joe@foo.com
                var parts = name.Split(new[] {'@'}, 2);
                ValidateDomainName(parts[1], paramName);
                name = parts[0];
            }

            if (!Regex.IsMatch(name, @"^[^. ""\/\\\[\]<>:|+=;?*]([^""\/\\\[\]<>:|+=;?*]{0,62})(?:\1|[^.])$",
                               RegexOptions.CultureInvariant))
                throw new ArgumentException(
                    @"Invalid Windows user name. Must contain 1 to 64 characters. Cannot start with space or period, and cannot end with a period. Cannot contain the following characters: "" / \ [ ] < > : | + = ; ? *",
                    paramName);
        }

        private static readonly Regex[] PasswordComplexityRegex = new[]
            {
                new Regex(@"[a-z]", RegexOptions.CultureInvariant), //lower
                new Regex(@"[A-Z]", RegexOptions.CultureInvariant), //upper
                new Regex(@"\d", RegexOptions.CultureInvariant), //digit
                new Regex(@"\W", RegexOptions.CultureInvariant), //symbol
            };

        public static void ValidatePasswordComplexity(string pass, string paramName = null)
        {
            if (pass == null || pass.Length < 8 || pass.Length > 127)
            {
                throw new ArgumentException("Invalid password. Must be between 8 and 127 characters.", paramName);
            }

            var complexity = PasswordComplexityRegex.Count(r => r.IsMatch(pass));
            if (complexity < 3)
            {
                throw new ArgumentException(
                    @"Invalid password. Must contain at least 3 of the following characters:\n" +
                    @"  - Lower case letter (a-z)\n" +
                    @"  - Upper case letter (A-Z)\n" +
                    @"  - Digit (0-9)\n" +
                    @"  - Other character (~!@#$%^&*()=+_[]{}\|;:.'"",<>/?)", 
                    paramName);
            }
        }

        /// <summary>
        /// Validates a LDAP Orgnaizational Unit (OU) distinguished name. It must contain 
        /// one or more OU components followed by one or more DC components.
        /// </summary>
        /// <param name="distinguishedName"></param>
        /// <param name="paramName"></param>
        public static void ValidateLdapOUName(string distinguishedName, string paramName = null)
        {
            //Remove escaped commas which will trip-up the regex...
            var strippedOU = distinguishedName.Replace(@"\\", "#").Replace(@"\,", "#");
            //Remove unescaped commas inside quoted values...
            if (strippedOU.Contains("\""))
            {
                //ignore escaped quotes...
                strippedOU = strippedOU.Replace(@"\""", "#");
                strippedOU = Regex.Replace(strippedOU, @"[""][^""]*[""]",
                                           m => m.Value.Substring(1, m.Value.Length - 2).Replace(",", "#"),
                                           RegexOptions.CultureInvariant);
            }

            //append a trailing comma in order to simplify the regex
            strippedOU += ",";
            if (!Regex.IsMatch(strippedOU, @"^(OU[ ]*=[ ]*([^,]+)[,][ ]*)+(DC[ ]*=[ ]*([^,]+)[,][ ]*)+$",
                               RegexOptions.IgnoreCase | RegexOptions.CultureInvariant))
                throw new ArgumentException("Invalid LDAP OU distinguished name.", paramName);
        }

        public static void ValidateEndpointName(string name, string paramName = null)
        {
            if (!Regex.IsMatch(name, @"^[a-zA-Z][a-zA-Z0-9 _-]{1,13}[a-zA-Z0-9]$", RegexOptions.CultureInvariant))
                throw new ArgumentException(
                    "Invalid Endpoint name. Must contain 3 to 15 letters, numbers, hyphens, spaces, and underscores. Must start with a letter and end with a letter or number.",
                    paramName);
        }

        public static void ValidateAvailabilitySetName(string name, string paramName = null)
        {
            if (!Regex.IsMatch(name, @"^[a-zA-Z][a-zA-Z0-9_-]{1,13}[a-zA-Z0-9]$", RegexOptions.CultureInvariant))
                throw new ArgumentException(
                    "Invalid Availability Set Name. Must contain 3 to 15 letters, numbers, hyphens, and underscores. Must start with a letter and end with a letter or number.",
                    paramName);
        }

        public static void ValidateDiskName(string name, string paramName = null)
        {
            if (!Regex.IsMatch(name, @"^[a-zA-Z]|[a-zA-Z][a-zA-Z0-9_.-]{0,254}[a-zA-Z0-9]$", RegexOptions.CultureInvariant))
                throw new ArgumentException(
                    "Invalid disk name. Name must contain 1 to 256 letters, numbers, periods, hyphens, and underscores. Must start with a letter and end with a letter or a number.",
                    paramName);
        }

        public static void ValidateDnsServerName(string name, string paramName = null)
        {
            if (!Regex.IsMatch(name, @"^[^0-9\s][^\s]{0,62}$", RegexOptions.CultureInvariant))
                throw new ArgumentException(
                    "Invalid DNS Server name. Name must contain 1 to 63 non-space characters. Must not start with a number.",
                    paramName);
        }

        public static void ValidateVnetName(string name, string paramName = null)
        {
            if (!Regex.IsMatch(name, @"^[^0-9\s][^\s]{0,62}$", RegexOptions.CultureInvariant))
                throw new ArgumentException(
                    "Invalid Virtual Network name. Name must contain 1 to 63 non-space characters. Must not start with a number.",
                    paramName);
        }

        public static void ValidateSubnetName(string name, string paramName = null)
        {
            if (!Regex.IsMatch(name, @"^[^0-9\s].{1,62}$", RegexOptions.CultureInvariant))
                throw new ArgumentException(
                    "Invalid Subnet name. Name must contain 2 to 63 characters. Must not start with a number or space.",
                    paramName);
        }

        public static void ValidateIpAddress(string ipAddress, string paramName = null)
        {
            IPAddress ip;
            if (!IPAddress.TryParse(ipAddress, out ip) ||
                ip.AddressFamily != AddressFamily.InterNetwork)
                throw new ArgumentException(
                    "Invalid IP Address.", paramName);
        }

        public static void ValidateAddressPrefix(string prefix, string paramName = null)
        {
            var parts = prefix.Split(new[] {'/'}, 2);
            IPAddress ip;
            int size;
            if (parts.Length != 2 ||
                (!IPAddress.TryParse(parts[0], out ip) || ip.AddressFamily != AddressFamily.InterNetwork) ||
                (!Int32.TryParse(parts[1], out size) || size < 0 || size > 32))
                throw new ArgumentException(
                    "Invalid address prefix. Must be a whole number between 0 and 32.", paramName);
        }

        public static void ValidateRoleSize(string size, string paramName = null)
        {
            if (String.IsNullOrEmpty(size) || 
                (!AzureRoleSizes.Contains(size, StringComparer.OrdinalIgnoreCase)))
                throw new ArgumentException(
                    String.Format(CultureInfo.InvariantCulture, "Invalid Role Size. Must be {0}",
                                  ConjoinWords(AzureRoleSizes, "or")),
                    paramName);
        }

        /// <summary>
        /// Joins a list of words using commas and a final conjuntion such as "or", "and", or "nor"
        /// </summary>
        /// <example>
        /// ConjoinWords(new []{"cat","dog","horse"}, "or"); // "cat, dog, or horse"
        /// ConjoinWords(new []{"cat","dog"}, "or"); // "cat or dog"
        /// ConjoinWords(new []{"cat"}, "or"); // "cat"
        /// </example> 
        /// <param name="values">The words to join. Null values are ignored.</param>
        /// <param name="conjunction">The final conjuntion such as "or", "and", or "nor"</param>
        /// <param name="delimiter">The delimiter between words. Default is comma (,).</param>
        /// <param name="addSpaces">True if spaces should be inserted after the delimiter and conjuntion.</param>
        public static string ConjoinWords(IEnumerable<string> values, string conjunction, string delimiter = ",",
                                         bool addSpaces = true)
        {
            if (values == null) throw new ArgumentNullException("values");
            if (conjunction == null) throw new ArgumentNullException("conjunction");
            if (delimiter == null) throw new ArgumentNullException("delimiter");
            if (addSpaces) delimiter += ' ';

            //ignore null values...
            var strings = values.Where(s => s != null).ToArray();

            if (conjunction == String.Empty || strings.Length < 2)
            {
                return String.Join(delimiter, strings);
            }

            var result = new StringBuilder();
            for (var i = 0; i < strings.Length - 1; i++)
            {
                if (i > 0)
                    result.Append(delimiter);
                result.Append(strings[i]);
            }
            if (strings.Length > 2) result.Append(delimiter); //"serial comma"
            else if (addSpaces) result.Append(' ');
            result.Append(conjunction);
            if (addSpaces) result.Append(' ');
            result.Append(strings[strings.Length - 1]);
            return result.ToString();
        }
    }
}