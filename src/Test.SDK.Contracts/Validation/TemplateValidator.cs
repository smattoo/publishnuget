using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using Brewmaster.TemplateSDK.Contracts.Fluent;
using Brewmaster.TemplateSDK.Contracts.Models;
using Brewmaster.TemplateSDK.Contracts.Serialization;
using JetBrains.Annotations;
using Newtonsoft.Json;

namespace Brewmaster.TemplateSDK.Contracts.Validation
{
    public class TemplateValidator : ITemplateValidator
    {
        private ValidationResults _resultCollector;
        private ITemplateSDKLiquidRenderer templateSdkLiquidRenderer;

        public ValidationResults Validate(string templateJson, IDictionary<string, string> @params = null)
        {
            _resultCollector = new ValidationResults();
            if (@params == null)
            {
                @params = new Dictionary<string, string>();
            }

            templateSdkLiquidRenderer = new TemplateSDKLiquidRenderer();

            var json = templateSdkLiquidRenderer.GenerateJson(templateJson, @params);
            
            var template = json.ToBrewmasterTemplate();

            ErrorIf(String.IsNullOrEmpty(template.Name), 
                    "Missing name for template.");

            ErrorIf(String.IsNullOrEmpty(template.Description),
                    "Missing description for template '{0}'", template.Name);

            ErrorIf(template.AffinityGroup == null,
                    "Affinity Group is required for template '{0}'", template.Name);

            Validate(template.Parameters);
            Validate(template.AffinityGroup);
            Validate(template.Network);
            Validate(template.StorageAccounts);
            Validate(template.CloudServices);
            Validate(template.DeploymentGroups);
            Validate(template.Credentials);
            //Validate(template.Certificates.Safe());
            Validate(template.ConfigSets);
            //Validate(template.Configurations);

            //
            //Perform high level validation...
            //

            //Warn if any referenced AffintyGroup is not defined.
            foreach (var virtualNetwork in template.Network.VirtualSites.Where(v => v != null))
            {
                if (!String.IsNullOrEmpty(virtualNetwork.AffinityGroup) &&
                    !template.AffinityGroup.Name.EqualNoCase(virtualNetwork.AffinityGroup))
                {
                    Warning("Affinity Group '{0}' referenced in Virtual Network '{1}' is not defined.",
                            virtualNetwork.AffinityGroup, virtualNetwork.Name);
                }
            }
            foreach (var storageAccount in template.StorageAccounts.Where(s => s != null))
            {
                if (!String.IsNullOrEmpty(storageAccount.AffinityGroup) &&
                    !template.AffinityGroup.Name.EqualNoCase(storageAccount.AffinityGroup))
                {
                    Warning("Affinity Group '{0}' referenced in Storage Account '{1}' is not defined.",
                            storageAccount.AffinityGroup, storageAccount.Name);
                }
            }
            foreach (var cloudService in template.CloudServices.Where(c => c != null))
            {
                if (!String.IsNullOrEmpty(cloudService.AffinityGroup) &&
                    !template.AffinityGroup.Name.EqualNoCase(cloudService.AffinityGroup))
                {
                    Warning("Affinity Group '{0}' referenced in Cloud Service '{1}' is not defined.",
                            cloudService.AffinityGroup, cloudService.Name);
                }
            }

            //Warn if any referenced Deployment Disk StorageAccount is not defined.
            foreach (var cloudService in template.CloudServices.Where(c => c != null && c.Deployment != null))
            {
                var diskStorageAccount = cloudService.Deployment.DiskStorageAccount;
                if (!String.IsNullOrEmpty(diskStorageAccount) &&
                    !template.StorageAccounts.Where(s => s != null)
                             .Any(s =>
                                 {
                                     return s.Name.EqualNoCase(diskStorageAccount);
                                 }))
                {
                    Warning(
                        "Default Disk Storage Account '{0}' referenced in Deployment '{1}' in Cloud Service '{2}' is not defined.",
                        diskStorageAccount, cloudService.Deployment.Name, cloudService.Name);
                }
            }

            //Warn if any referenced Deployment VNet is not defined.
            foreach (var cloudService in template.CloudServices
                                                 .Where(c => c != null && c.Deployment != null)
                                                 .Where(c => c.Deployment.VirtualNetwork != null))
            {
                var vnetName = cloudService.Deployment.VirtualNetwork;
                var vnet = template.Network.VirtualSites.Where(v => v != null)
                                   .FirstOrDefault(v =>
                                       {
                                           return v.Name.EqualNoCase(vnetName);
                                       });
                if (vnet == null)
                {
                    Warning(
                        "Virtual Network '{0}' referenced in Deployment '{1}' in Cloud Service '{2}' is not defined.",
                        vnetName, cloudService.Deployment.Name, cloudService.Name);
                }
                else
                {
                    //make sure the referenced VM Subnets are defined.
                    foreach (var virtualMachine in cloudService.Deployment
                                                               .VirtualMachines.Safe()
                                                               .Where(v => v != null))
                    {
                        if (!virtualMachine.Subnets.Safe().Any())
                        {
                            Warning(
                                "Virtual Machine '{0}' in Cloud Service '{1}' is not assigned to a subnet.",
                                virtualMachine.Name, cloudService.Name);
                        }
                        else
                        {
                            foreach (var subnetName in virtualMachine.Subnets.Safe().Where(s => s != null))
                            {
                                if (!vnet.Subnets.Safe().Where(s => s != null).Any(s =>
                                    {
                                        return s.Name.EqualNoCase(subnetName);
                                    }))
                                {
                                    Warning(
                                        "Subnet '{0}' referenced in Virtual Machine '{1}' in Cloud Service '{2}' is not defined.",
                                        subnetName, virtualMachine.Name, cloudService.Name);
                                }
                            }
                        }
                    }
                }
            }

            //make sure the referenced virtualMachine credentials are defined
            var credentialIds = template.Credentials.Where(c => c != null).Select(c => c.Name).ToList();
            foreach (var cloudService in template.CloudServices
                                                 .Where(c => c != null && c.Deployment != null))
            {
                foreach (var virtualMachine in cloudService.Deployment
                                                           .VirtualMachines.Safe()
                                                           .Where(v => v != null && v.WindowsConfigSet != null))
                {
                    var windowsConfig = virtualMachine.WindowsConfigSet;
                    if (!String.IsNullOrEmpty(windowsConfig.LocalAdminCredentialId) &&
                        !credentialIds.Contains(windowsConfig.LocalAdminCredentialId,
                                                StringComparer.OrdinalIgnoreCase))
                    {
                        //look for an implicit ":local" credential
                        if (windowsConfig.LocalAdminCredentialId.Contains(":") ||
                            !credentialIds.Contains(windowsConfig.LocalAdminCredentialId + ":local",
                                                    StringComparer.OrdinalIgnoreCase))
                        {
                            Error(
                                "Local Admin Credential '{0}' referenced in Virtual Machine '{1}' in Cloud Service '{2}' is not defined.",
                                windowsConfig.LocalAdminCredentialId, virtualMachine.Name, cloudService.Name);
                        }
                    }

                    var domainJoinSettings = windowsConfig.DomainJoinSettings;
                    if (domainJoinSettings != null &&
                        !String.IsNullOrEmpty(domainJoinSettings.CredentialId) &&
                        !credentialIds.Contains(domainJoinSettings.CredentialId,
                                                StringComparer.OrdinalIgnoreCase))
                    {
                        //look for an implicit ":domain" credential
                        if (domainJoinSettings.CredentialId.Contains(":") ||
                            !credentialIds.Contains(domainJoinSettings.CredentialId + ":domain",
                                                    StringComparer.OrdinalIgnoreCase))
                        {
                            Error(
                                "Domain Join Credential '{0}' referenced in Virtual Machine '{1}' in Cloud Service '{2}' is not defined.",
                                domainJoinSettings.CredentialId, virtualMachine.Name, cloudService.Name);
                        }
                    }
                }
            }

            //make sure the referenced virtualMachine DeploymentGroup is defined
            foreach (var cloudService in template.CloudServices
                                                 .Where(c => c != null && c.Deployment != null))
            {
                foreach (var virtualMachine in cloudService.Deployment.VirtualMachines.Safe().Where(v => v != null))
                {
                    if (!DeploymentGroup.IsMatch(virtualMachine.DeploymentGroup, DeploymentGroup.DefaultName) &&
                        !template.DeploymentGroups.Any(
                            dg => DeploymentGroup.IsMatch(dg.Name, virtualMachine.DeploymentGroup)))
                    {
                        Error(
                            "Deployment Group '{0}' referenced in Virtual Machine '{1}' in Cloud Service '{2}' is not defined.",
                            virtualMachine.DeploymentGroup, virtualMachine.Name, cloudService.Name);
                    }
                }
            }

            //make sure the referenced virtualMachine ConfigSets are defined
            foreach (var cloudService in template.CloudServices.Where(c => c != null && c.Deployment != null))
            {
                foreach (var virtualMachine in cloudService.Deployment
                                                           .VirtualMachines.Safe()
                                                           .Where(v => v != null && v.ConfigSets != null))
                {
                    foreach (var configSet in virtualMachine.ConfigSets)
                    {
                        if (!String.IsNullOrEmpty(configSet) &&
                            !template.ConfigSets.Any(c =>
                                {
                                    return c.Name.EqualNoCase(configSet);
                                }))
                        {
                            Error(
                                "ConfigSet '{0}' referenced in Virtual Machine '{1}' in Cloud Service '{2}' is not defined.",
                                configSet, virtualMachine.Name, cloudService.Name);
                        }
                    }
                }
            }

            //make sure the ConfigSet references are defined.
            foreach (var configSet in template.ConfigSets.Where(c => c != null))
            {
                //foreach (var certificateId in configSet.CertificateIds.Safe())
                //{
                //    if (!String.IsNullOrEmpty(certificateId) &&
                //        !template.Certificates.Any(c =>
                //            {
                //                return c.Name.EqualNoCase(certificateId);
                //            }))
                //    {
                //        Error(
                //            "Certificate '{0}' referenced in ConfigSet '{1}' is not defined.",
                //            certificateId, configSet.Name);
                //    }
                //}

                foreach (var configurationId in configSet.ConfigurationIds.Safe())
                {
                    if (!String.IsNullOrEmpty(configurationId) &&
                        !template.Configurations.Any(c =>
                            {
                                return c.Name.EqualNoCase(configurationId);
                            }))
                    {
                        Error(
                            "Configuration '{0}' referenced in ConfigSet '{1}' is not defined.",
                            configurationId, configSet.Name);
                    }
                }
            }
            return _resultCollector;
        }

        public IDictionary<string, string> GenerateParamsFromTemplate(string templateJson)
        {
            templateSdkLiquidRenderer = new TemplateSDKLiquidRenderer();

            if (string.IsNullOrEmpty(templateJson)) throw new ArgumentNullException("templateJson");

            var templateJsonWithEmptyLiquidParams = templateSdkLiquidRenderer.GenerateJson(templateJson, new Dictionary<string, string>());

            var templateWithEmptyLiquidParams = JsonConvert.DeserializeObject<BrewmasterTemplate>(templateJsonWithEmptyLiquidParams);
            var paramsWithValues = templateWithEmptyLiquidParams.Parameters.ToDictionary(p => p.Name,
                                                                                   p => p.ProvideValues());
            return paramsWithValues;
        }

        private void Validate(Parameter[] parameters)
        {
            if (parameters == null) throw new ArgumentNullException("parameters");

            foreach (var parameter in parameters)
            {
                if (parameter == null)
                {
                    Error("Null template parameter.");
                    continue;
                }

                Validate(parameter);
            }

            FindDuplicates(
                parameters.Where(o => o != null && !String.IsNullOrEmpty(o.Name)),
                dup => Error("Parameter '{0}' is present multiple times.", dup.Name));
        }

        private void Validate(Parameter parameter)
        {
            if (parameter == null) throw new ArgumentNullException("parameter");

            //Validate Name
            Try(() => ValidationHelpers.ValidateParameterName(parameter.Name, parameter.GetType().Name),
                e => Error("Invalid name for parameter '{0}': {1}", parameter.GetType().Name, e.Message));

            //Validate Description
            if (String.IsNullOrWhiteSpace(parameter.Description))
            {
                Error("A description is required for parameter '{0}'.", parameter.Name);
            }

            //Validate Minimum and Maximum
            var checkMinMax = (parameter.Minimum.HasValue || parameter.Maximum.HasValue);
            if (checkMinMax)
            {
                if (parameter.Type == ParameterType.String)
                {
                    checkMinMax &= !ErrorIf(parameter.Minimum != (int?) parameter.Minimum,
                                            "Minimum length of parameter '{0}' must be a whole number.",
                                            parameter.Name);
                    checkMinMax &= !ErrorIf(parameter.Minimum < Parameter.MinimumStringLength,
                                            "Minimum length of parameter '{0}' must not be less than {1}.",
                                            parameter.Name, Parameter.MinimumStringLength);

                    checkMinMax &= !ErrorIf(parameter.Maximum != (int?) parameter.Maximum,
                                            "Maximum length of parameter '{0}' must be a whole number.",
                                            parameter.Name);
                    checkMinMax &= !ErrorIf(parameter.Maximum > Parameter.MaximumStringLength,
                                            "Maximum length of parameter '{0}' must not be greater than {1}.",
                                            parameter.Name, Parameter.MaximumStringLength);

                    checkMinMax &= !ErrorIf(parameter.Minimum > parameter.Maximum,
                                            "Minimum length of parameter '{0}' must not be greater than the maximum length.",
                                            parameter.Name);
                }
                else if (parameter.Type == ParameterType.Number)
                {
                    checkMinMax &= !ErrorIf(parameter.Minimum > parameter.Maximum,
                                            "Minimum value of parameter '{0}' must not be greater than the maximum value.",
                                            parameter.Name);
                }
                else
                {
                    checkMinMax = false;
                    Warning("Parameter '{0}' does not support minimum or maximum value.", parameter.Name);
                }
            }

            //Validate Default
            var defaultValues = new string[0];
            if (parameter.Default != null)
            {
                if (parameter.IsList == true)
                {
                    Try(() => defaultValues = ParseCsvLine(parameter.Default),
                        e => Error("Error parsing default value list for parameter '{0}': {1}",
                                   parameter.Name, e.Message));
                }
                else
                {
                    defaultValues = new[] {parameter.Default};
                }

                foreach (var defaultValue in defaultValues)
                {
                    if (parameter.Type == ParameterType.String)
                    {
                        if (checkMinMax)
                        {
                            WarningIf(defaultValue.Length < parameter.Minimum,
                                      "Length of default value of parameter '{0}' is shorter than the minimum.",
                                      parameter.Name);
                            WarningIf(defaultValue.Length > parameter.Maximum,
                                      "Length of default value of parameter '{0}' is longer than the maximum.",
                                      parameter.Name);
                        }
                    }
                    else if (parameter.Type == ParameterType.Number)
                    {
                        //valid that it is a number
                        double defaultNumber;
                        if (!TryParseDouble(defaultValue, out defaultNumber))
                        {
                            Warning("Default value of parameter '{0}' is not a valid number.",
                                    parameter.Name);
                            continue;
                        }

                        if (checkMinMax)
                        {
                            WarningIf(defaultNumber < parameter.Minimum,
                                      "Default value of parameter '{0}' is less than the minimum value.",
                                      parameter.Name);

                            WarningIf(defaultNumber > parameter.Maximum,
                                      "Default value of parameter '{0}' is greater than the maximum value.",
                                      parameter.Name);
                        }
                    }
                    else if (parameter.Type == ParameterType.Boolean)
                    {
                        //valid that it is a boolean
                        bool defaultBool;
                        if (!TryParseBoolean(defaultValue, out defaultBool))
                        {
                            Warning("Default value of parameter '{0}' is not a valid boolean.",
                                    parameter.Name);
                        }
                    }
                    else if (parameter.Type == ParameterType.Date)
                    {
                        //valid that it is a boolean
                        DateTime defaultDate;
                        if (!TryParseDate(defaultValue, out defaultDate))
                        {
                            Warning("Default value of parameter '{0}' is not a valid date.",
                                    parameter.Name);
                        }
                    }
                }
            }

            //Validate AllowedRegex
            if (parameter.AllowedRegex != null)
            {
                Try(() =>
                    {
                        var regex = new Regex(parameter.AllowedRegex, RegexOptions.CultureInvariant,
                                              TimeSpan.FromSeconds(1));

                        //make sure allowed values match the regex...
                        if (parameter.AllowedValues != null &&
                            parameter.AllowedValues.Any(allowedValue => !regex.IsMatch(allowedValue)))
                        {
                            Warning("Not all AllowedValues of parameter '{0}' match AllowedRegex.",
                                    parameter.Name);
                        }

                        //make sure default values match the regex...
                        if (defaultValues.Any(value => !regex.IsMatch(value)))
                        {
                            Warning("The default value of parameter '{0}' does not match AllowedRegex.",
                                    parameter.Name);
                        }
                    },
                    ex => Error("Error validating AllowedRegex in parameter '{0}': {1}",
                                parameter.Name, ex.Message));
            }

            //Validate AllowedValues
            if (parameter.AllowedValues != null)
            {
                if (!parameter.AllowedValues.Any())
                {
                    Error("Parameter '{0}' has an empty AllowedValues list.",
                          parameter.Name);
                }
                else if (parameter.AllowedValues.Any(v => v == null))
                {
                    Error("Parameter '{0}' contains null values in the AllowedValues list.",
                          parameter.Name);
                }
                else
                {
                    switch (parameter.Type)
                    {
                        case ParameterType.Number:
                            double number;
                            ErrorIf(parameter.AllowedValues.Any(v => !TryParseDouble(v, out number)),
                                    "Parameter '{0}' contains invalid number(s) in the AllowedValues list.",
                                    parameter.Name);
                            break;

                        case ParameterType.Boolean:
                            bool boolean;
                            ErrorIf(parameter.AllowedValues.Any(v => !TryParseBoolean(v, out boolean)),
                                    "Parameter '{0}' contains invalid boolean(s) in the AllowedValues list.",
                                    parameter.Name);
                            break;

                        case ParameterType.Date:
                            DateTime date;
                            ErrorIf(parameter.AllowedValues.Any(v => !TryParseDate(v, out date)),
                                    "Parameter '{0}' contains invalid date(s) in the AllowedValues list.",
                                    parameter.Name);
                            break;

                        default:
                            //if NOT using Regex, make sure the default values are in the AllowedValues list...
                            if (parameter.AllowedRegex == null &&
                                defaultValues.Except(parameter.AllowedValues, StringComparer.OrdinalIgnoreCase).Any())
                            {
                                Warning("Default value of parameter '{0}' is not in the AllowedValues list.",
                                        parameter.Name);
                            }
                            break;
                    }
                }
            }
        }

        private void Validate(AffinityGroup affinityGroup)
        {
            if (affinityGroup == null) throw new ArgumentNullException("affinityGroup");

            //Validate Name
            Try(() => ValidationHelpers.ValidateAffinityGroupName(affinityGroup.Name, affinityGroup.GetType().Name),
                e => Error("Invalid name for Affinity Group '{0}': {1}", affinityGroup.GetType().Name, e.Message));

            Try(() => ValidationHelpers.ValidateRegionName(affinityGroup.Region, affinityGroup.GetType().Name),
                e => Error("Invalid region for Affinity Group '{0}': {1}", affinityGroup.GetType().Name, e.Message));

            ErrorIf(affinityGroup.Label != null && affinityGroup.Label.Trim() == "",
                    "Must specify a non-empty label for Affinity Group '{0}'.", affinityGroup.Name);
        }

        private void Validate(Network network)
        {
            if (network == null) throw new ArgumentNullException("network");

            Validate(network.DnsServers);
            Validate(network.VirtualSites);
            Validate(network.LocalSites);

            //
            //Perform high level validation...
            //

            //Warn if any referenced VNet DNS servers are not defined.
            foreach (var virtualNetwork in network.VirtualSites.Where(v => v != null))
            {
                foreach (var dnsServer in virtualNetwork.DnsServers.Safe())
                {
                    if (!String.IsNullOrEmpty(dnsServer) &&
                        !network.DnsServers.Safe().Where(dns => dns != null)
                                .Any(dns => dns.Name.EqualNoCase(dnsServer)))
                    {
                        Warning("DNS Server reference '{0}' in Virtual Network '{1}' is not defined.",
                                dnsServer, virtualNetwork.Name);
                    }
                }
            }

            //Warn if any referenced VNet Local Sites are not defined.
            foreach (var virtualNetwork in network.VirtualSites.Where(v => v != null))
            {
                if (String.IsNullOrEmpty(virtualNetwork.LocalSiteRefConnection)) continue;

                var localSite =
                    network.LocalSites.Safe().Where(s => s != null)
                           .SingleOrDefault(s => s.Name.EqualNoCase(virtualNetwork.LocalSiteRefConnection));

                if (localSite == null)
                {
                    Warning("Local Network reference '{0}' in Virtual Network '{1}' is not defined.",
                            virtualNetwork.LocalSiteRefConnection, virtualNetwork.Name);
                }
                else
                {
                    //Validate that Local and Virtual AddressSpaces do not overlap...
                    var virtualSpaces = new List<CIDRAddressSpace>(virtualNetwork.AddressSpace.Length);
                    foreach (var space in virtualNetwork.AddressSpace)
                    {
                        CIDRAddressSpace cidr;
                        if (CIDRAddressSpace.TryParse(space, out cidr))
                            virtualSpaces.Add(cidr);
                    }

                    foreach (var space in localSite.AddressSpace)
                    {
                        CIDRAddressSpace localSpace;
                        if (!CIDRAddressSpace.TryParse(space, out localSpace)) continue;

                        var overlap = virtualSpaces.Where(vs => localSpace.Overlaps(vs)).ToList();
                        if (overlap.Any())
                        {
                            Error(
                                "Address spaces conflict via Local Network reference '{0}' in Virtual Network '{1}'. Local address space {2} overlaps virtual address space(s) {3}.",
                                virtualNetwork.LocalSiteRefConnection, virtualNetwork.Name, localSpace,
                                ValidationHelpers.ConjoinWords(overlap.Select(s => s.ToString()), "and"));
                        }
                    }
                }
            }
        }

        private void Validate(DnsServer[] dnsServers)
        {
            if (dnsServers == null) throw new ArgumentNullException("dnsServers");

            foreach (var dnsServer in dnsServers)
            {
                if (dnsServer == null)
                {
                    Error("Null template DNS Server.");
                    continue;
                }

                Validate(dnsServer);
            }

            FindDuplicates(
                dnsServers.Where(o => o != null && !String.IsNullOrEmpty(o.Name)),
                dup => Error("DNS Server '{0}' is present multiple times.", dup.Name));
        }

        private void Validate(DnsServer dnsServer)
        {
            if (dnsServer == null) throw new ArgumentNullException("dnsServer");

            Try(() => ValidationHelpers.ValidateDnsServerName(dnsServer.Name, dnsServer.GetType().Name),
                e => Error("Invalid name for DNS Server '{0}': {1}", dnsServer.GetType().Name, e.Message));

            Try(() => ValidationHelpers.ValidateIpAddress(dnsServer.IPAddress, "IP Address"),
                e => Error("Invalid IP Address for DNS Server '{0}': {1}", dnsServer.GetType().Name, e.Message));
        }

        private void Validate(LocalNetworkSite[] localNetworks)
        {
            if (localNetworks == null) throw new ArgumentNullException("localNetworks");

            foreach (var net in localNetworks)
            {
                if (net == null)
                {
                    Error("Null Local Network.");
                    continue;
                }

                Validate(net);
            }

            FindDuplicates(
                localNetworks.Where(o => o != null && !String.IsNullOrEmpty(o.Name)),
                dup => Error("Local Network '{0}' is present multiple times.", dup.Name));
        }

        private void Validate(LocalNetworkSite network)
        {
            if (network == null) throw new ArgumentNullException("network");

            var thisContext = String.Format(CultureInfo.InvariantCulture, "Local Network '{0}'", network.Name);

            Try(() => ValidationHelpers.ValidateVnetName(network.Name, network.GetType().Name),
                e => Error("Invalid name for {0}: {1}", thisContext, e.Message));

            IPAddress vpnAddress;
            ErrorIf(network.VPNGatewayAddress == null || 
                    !IPAddress.TryParse(network.VPNGatewayAddress, out vpnAddress),
                    "Invalid VPN Gateway Address for {0}", thisContext);

            ErrorIf(network.AddressSpace.Length == 0,
                    "Must specify at least one Address Space for {0}", thisContext);

            var addressSpaces = new List<CIDRAddressSpace>(network.AddressSpace.Length);
            foreach (var addressSpace in network.AddressSpace)
            {
                var space = addressSpace;
                Try(() => addressSpaces.Add(CIDRAddressSpace.Parse(space)),
                    e => Error("Error parsing Address Space '{0}' of {1}: {2}", space, thisContext, e.Message));
            }

            //Validate that AddressSpaces do not overlap...
            foreach (var addressSpace in addressSpaces)
            {
                var overlap = addressSpaces.Where(s => !ReferenceEquals(s, addressSpace))
                                           .Where(s => s.Overlaps(addressSpace))
                                           .ToList();
                if (overlap.Any())
                {
                    Error("Address Space {0} overlaps address space(s) {1} in {2}.",
                          addressSpace, ValidationHelpers.ConjoinWords(overlap.Select(s => s.ToString()), "and"),
                          thisContext);
                }
            }
        }

        private void Validate(VirtualNetworkSite[] virtualNetworks)
        {
            if (virtualNetworks == null) throw new ArgumentNullException("virtualNetworks");

            foreach (var vnet in virtualNetworks)
            {
                if (vnet == null)
                {
                    Error("Null Virtual Network.");
                    continue;
                }

                Validate(vnet);
            }

            FindDuplicates(
                virtualNetworks.Where(o => o != null && !String.IsNullOrEmpty(o.Name)),
                dup => Error("Virtual Network '{0}' is present multiple times.", dup.Name));
        }

        private void Validate(VirtualNetworkSite vnet)
        {
            if (vnet == null) throw new ArgumentNullException("vnet");

            var thisContext = String.Format(CultureInfo.InvariantCulture, "Virtual Network '{0}'", vnet.Name);

            Try(() => ValidationHelpers.ValidateVnetName(vnet.Name, vnet.GetType().Name),
                e => Error("Invalid name for {0}: {1}", thisContext, e.Message));

            ErrorIf(String.IsNullOrEmpty(vnet.AffinityGroup),
                    "Must specify an Affinity Group for {0}", thisContext);

            ErrorIf(vnet.DnsServers != null && vnet.DnsServers.Any(String.IsNullOrEmpty),
                    "Empty DNS Server reference found in {0}", thisContext);
            FindDuplicates(
                vnet.DnsServers.Safe().Where(d => !String.IsNullOrEmpty(d)),
                dup => Error("DNS Server reference '{0}' in {1} is present multiple times.", dup, thisContext));

            var addressSpaces = new List<CIDRAddressSpace>(vnet.AddressSpace.Length);
            foreach (var addressSpace in vnet.AddressSpace)
            {
                var space = addressSpace;
                Try(() => addressSpaces.Add(CIDRAddressSpace.Parse(space)),
                    e => Error("Error parsing Address Space '{0}' of {1}: {2}", space, thisContext, e.Message));
            }

            //Validate that AddressSpaces do not overlap...
            foreach (var addressSpace in addressSpaces)
            {
                var overlap = addressSpaces.Where(s => !ReferenceEquals(s, addressSpace))
                                           .Where(s => s.Overlaps(addressSpace))
                                           .ToList();
                if (overlap.Any())
                {
                    Error("Address Space {0} overlaps address space(s) {1} in {2}.",
                          addressSpace, ValidationHelpers.ConjoinWords(overlap.Select(s => s.ToString()), "and"),
                          thisContext);
                }
            }

            Validate(vnet.Subnets, thisContext);

            //Validate that all Subnets are inside an AddressSpaces...
            var subnetAddressSpaces = new List<Tuple<Subnet, CIDRAddressSpace>>();
            foreach (var subnet in vnet.Subnets.Where(s => s != null))
            {
                CIDRAddressSpace subnetAddressSpace;
                if (!CIDRAddressSpace.TryParse(subnet.AddressPrefix, out subnetAddressSpace))
                    continue; //ignore invalid addresses; ValidateSubnets() will report them.

                if (addressSpaces.All(addressSpace => !addressSpace.Contains(subnetAddressSpace)))
                {
                    Error("Subnet '{0}' is not in the Address Space of {1}.",
                          subnet.Name, thisContext);
                }
                else
                {
                    subnetAddressSpaces.Add(Tuple.Create(subnet, subnetAddressSpace));
                }
            }

            //Validate that Subnets do not overlap
            foreach (var subnetAddressSpace in subnetAddressSpaces)
            {
                var overlappingSubnets =
                    subnetAddressSpaces.Where(s => !ReferenceEquals(s, subnetAddressSpace))
                                       .Where(s => s.Item2.Overlaps(subnetAddressSpace.Item2))
                                       .ToList();
                if (overlappingSubnets.Any())
                {
                    Error("Subnet '{0}' overlaps subnet(s) {1} in {2}.",
                          subnetAddressSpace.Item1.Name,
                          ValidationHelpers.ConjoinWords(overlappingSubnets.Select(s => "'" + s.Item1.Name + "'"), "and"),
                          thisContext);
                }
            }

            //Validate that VPN Client AddressSpaces do not overlap...
            var vpnClientAddressSpaces = new List<CIDRAddressSpace>(vnet.VPNClientAddressSpaces.Length);
            foreach (var vpnSpace in vnet.VPNClientAddressSpaces)
            {
                var space = vpnSpace;
                Try(() => vpnClientAddressSpaces.Add(CIDRAddressSpace.Parse(space)),
                    e =>
                    Error("Error parsing VPN Client Address Space '{0}' of {1}: {2}", vpnSpace, thisContext, e.Message));
            }

            foreach (var addressSpace in vpnClientAddressSpaces)
            {
                var overlap = vpnClientAddressSpaces.Where(s => !ReferenceEquals(s, addressSpace))
                                                    .Where(s => s.Overlaps(addressSpace))
                                                    .ToList();
                if (overlap.Any())
                {
                    Error("VPN Client Address Space {0} overlaps client address space(s) {1} in {2}.",
                          addressSpace, ValidationHelpers.ConjoinWords(overlap.Select(s => s.ToString()), "and"),
                          thisContext);
                }
            }

            var vpnClientAddresses = vpnClientAddressSpaces.Sum(s => s.MaxAddresses());
            ErrorIf(vpnClientAddresses > ValidationHelpers.VPNClientMaxAddresses,
                    "VPN Client Address Space is too large in {0}. Total addresses must less than {1}.",
                    thisContext, ValidationHelpers.VPNClientMaxAddresses+1);

            //Make sure Point-to-Site and Site-to-Site connections have a Gateway subnet...
            if (vnet.VPNClientAddressSpaces.Safe().Any() ||
                !String.IsNullOrEmpty(vnet.LocalSiteRefConnection))
            {
                ErrorIf(!vnet.Subnets.Where(s => s != null).Any(s => s.Name.EqualNoCase("GatewaySubnet")),
                        "Point-to-Site and Site-to-Site connections require a 'GatewaySubnet' in {0}", 
                        thisContext);
            }
        }

        private void Validate(Subnet[] subnets, string context)
        {
            if (subnets == null) throw new ArgumentNullException("subnets");
            if (context == null) throw new ArgumentNullException("context");

            foreach (var subnet in subnets)
            {
                if (subnet == null)
                {
                    Error("Null Subnet in {0}.", context);
                    continue;
                }

                Validate(subnet, context);
            }

            FindDuplicates(
                subnets.Where(o => o != null && !String.IsNullOrEmpty(o.Name)),
                dup => Error("Subnet '{0}' is present multiple times in {1}.",
                             dup.Name, context));
        }

        private void Validate(Subnet subnet, string context)
        {
            if (subnet == null) throw new ArgumentNullException("subnet");
            if (context == null) throw new ArgumentNullException("context");

            Try(() => ValidationHelpers.ValidateSubnetName(subnet.Name, subnet.GetType().Name),
                e => Error("Invalid Subnet name '{0}' in {1}: {2}",
                           subnet.Name, context, e.Message));

            Try(() => CIDRAddressSpace.Parse(subnet.AddressPrefix),
                e => Error("Error parsing Address Space of Subnet '{0}' in {1}: {2}",
                           subnet.Name, context, e.Message));
        }

        private void Validate(StorageAccount[] storageAccounts)
        {
            if (storageAccounts == null) throw new ArgumentNullException("storageAccounts");

            foreach (var storageAccount in storageAccounts)
            {
                if (storageAccount == null)
                {
                    Error("Null Storage Account.");
                    continue;
                }

                Validate(storageAccount);
            }

            FindDuplicates(
                storageAccounts.Where(o => o != null && !String.IsNullOrEmpty(o.Name)),
                dup => Error("Storage Account '{0}' is present multiple times.",
                             dup.Name));
        }

        private void Validate(StorageAccount storageAccount)
        {
            if (storageAccount == null) throw new ArgumentNullException("storageAccount");

            var context = String.Format(CultureInfo.InvariantCulture, "Storage Account '{0}'", storageAccount.Name);

            Try(() => ValidationHelpers.ValidateStorageAccountName(storageAccount.Name, storageAccount.GetType().Name),
                e => Error("Invalid name for {0}: {1}", context, e.Message));

            ErrorIf(String.IsNullOrEmpty(storageAccount.AffinityGroup) &&
                    String.IsNullOrEmpty(storageAccount.Region),
                    "Must specify a Region or, preferably, an Affinity Group for {0}", context);

            if (!String.IsNullOrEmpty(storageAccount.Region))
            {
                Try(() => ValidationHelpers.ValidateRegionName(storageAccount.Region, "Region"),
                    e => Error("Invalid region for {0}: {1}", context, e.Message));
            }

            WarningIf(String.IsNullOrEmpty(storageAccount.AffinityGroup),
                      "{0} is not assigned to an Affinity Group.", context);

            ErrorIf(storageAccount.Label != null && storageAccount.Label.Trim() == "",
                    "Must specify a non-empty label for {0}.", context);
        }

        private void Validate(CloudService[] cloudServices)
        {
            if (cloudServices == null) throw new ArgumentNullException("cloudServices");

            foreach (var cloudService in cloudServices)
            {
                if (cloudService == null)
                {
                    Error("Null Cloud Service.");
                    continue;
                }

                Validate(cloudService);
            }

            FindDuplicates(
                cloudServices.Where(o => o != null && !String.IsNullOrEmpty(o.Name)),
                dup => Error("Cloud Service '{0}' is present multiple times.",
                             dup.Name));
        }

        private void Validate(CloudService cloudService)
        {
            if (cloudService == null) throw new ArgumentNullException("cloudService");

            var context = String.Format(CultureInfo.InvariantCulture, "Cloud Service '{0}'", cloudService.Name);

            Try(() => ValidationHelpers.ValidateCloudServiceName(cloudService.Name, cloudService.GetType().Name),
                e => Error("Invalid name for {0}: {1}", context, e.Message));

            ErrorIf(String.IsNullOrEmpty(cloudService.AffinityGroup) &&
                    String.IsNullOrEmpty(cloudService.Region),
                    "Must specify a Region or, preferably, an Affinity Group for {0}", context);

            if (!String.IsNullOrEmpty(cloudService.Region))
            {
                Try(() => ValidationHelpers.ValidateRegionName(cloudService.Region, "Region"),
                    e => Error("Invalid region for {0}: {1}", context, e.Message));
            }

            WarningIf(String.IsNullOrEmpty(cloudService.AffinityGroup),
                      "{0} is not assigned to an Affinity Group.", context);

            ErrorIf(cloudService.Label != null && cloudService.Label.Trim() == "",
                    "Must specify a non-empty label for {0}.", context);

            if (cloudService.Deployment != null)
            {
                Validate(cloudService.Deployment, context);
            }
        }

        private void Validate(VmDeployment deployment, string context)
        {
            if (deployment == null) throw new ArgumentNullException("deployment");

            var thisContext = String.Format("Deployment '{0}' in Cloud Service '{1}'", deployment.Name, context);


            //Try(() => ValidationHelpers.ValidateDeploymentName(deployment.Name, deployment.GetType().Name),
            //    e => Error("Invalid name for {0}: {1}", deployment.GetType().Name, e.Message));
            
            //ErrorIf(, "Must specify a non-empty label for {0}.", thisContext);

            if (deployment.DnsServers.Safe().Any())
            {
                ErrorIf(deployment.VirtualNetwork == null,
                        "Must specify a Virtual Network when using deployment-specific DNS Servers in {0}.",
                        thisContext);

                Validate(deployment.DnsServers);
            }

            Validate(deployment.VirtualMachines, context);

            if (String.IsNullOrEmpty(deployment.DiskStorageAccount))
            {
                var virtualMachinesRequiringDefaultStorageAccount =
                    deployment.VirtualMachines
                              .Where(vm => vm != null)
                              .Where(
                                  vm =>
                                  (vm.OsVirtualDisk != null &&
                                   vm.OsVirtualDisk.MediaLink == null && String.IsNullOrEmpty(vm.OsVirtualDisk.DiskName)) ||
                                  (vm.DataVirtualDisks != null &&
                                   vm.DataVirtualDisks.Any(d => d.MediaLink == null && String.IsNullOrEmpty(d.DiskName))));

                ErrorIf(virtualMachinesRequiringDefaultStorageAccount.Any(),
                        "One or more new disks require a Media Link in {0}. " +
                        "Specify a default storage account and the links will be autogenerated",
                        thisContext);
            }
        }

        private void Validate(VirtualMachine[] virtualMachines, string context)
        {
            if (virtualMachines == null) throw new ArgumentNullException("virtualMachines");
            if (context == null) throw new ArgumentNullException("context");

            ErrorIf(!virtualMachines.Any(),
                    "Must have at least one Virtual Machine in {0}.", context);

            foreach (var virtualMachine in virtualMachines)
            {
                if (virtualMachine == null)
                {
                    Error("Null Virtual Machine in {0}.", context);
                    continue;
                }

                Validate(virtualMachine, context);
            }

            FindDuplicates(
                virtualMachines.Where(o => o != null && !String.IsNullOrEmpty(o.Name)),
                dup => Error("Virtual Machine '{0}' is present multiple times in {1}",
                             dup.Name, context));
        }

        private void Validate(VirtualMachine virtualMachine, string context)
        {
            if (virtualMachine == null) throw new ArgumentNullException("virtualMachine");
            if (context == null) throw new ArgumentNullException("context");

            var thisContext = String.Format("Virtual Machine '{0}' in {1}", virtualMachine.Name, context);

            //Name is required
            Try(() => ValidationHelpers.ValidateVirtualMachineName(virtualMachine.Name, virtualMachine.GetType().Name),
                ex => Error("Invalid Role Name for {0}: {1}", thisContext, ex.Message));

            //RoleSize is required
            Try(() => ValidationHelpers.ValidateRoleSize(virtualMachine.RoleSize, "Role Size"),
                ex => Error("Invalid Role Size for {0}: {1}", thisContext, ex.Message));

            //AvailabilitySet is optional.
            if (virtualMachine.AvailabilitySet == null)
            {
                Warning("{0} is not assigned to an Availability Set.", thisContext);
            }
            else
            {
                Try(() => ValidationHelpers.ValidateAvailabilitySetName(virtualMachine.AvailabilitySet, "Availability Set"),
                    ex => Error("Invalid Availability Set for {0}: {1}", thisContext, ex.Message));
            }

            //WindowsConfig is required.
            if (!ErrorIf(virtualMachine.WindowsConfigSet == null,
                         "Missing Windows Configuration for {0}.", thisContext))
            {
                Validate(virtualMachine.WindowsConfigSet, thisContext);
            }

            //OSDisk is optional. Cannot create new VM without it, but can use an existing VM
            if (virtualMachine.OsVirtualDisk != null)
            {
                Validate(virtualMachine.OsVirtualDisk, thisContext);
            }

            //DataDisks are optional
            if (virtualMachine.DataVirtualDisks != null)
            {
                Validate(virtualMachine.DataVirtualDisks, thisContext);
            }

            //Endpoints are optional
            if (virtualMachine.Endpoints != null)
            {
                Validate(virtualMachine.Endpoints, thisContext);
            }

            //ConfigSets must be unique
            ErrorIf(virtualMachine.ConfigSets.Safe().Any(String.IsNullOrEmpty),
                    "Empty ConfigSet reference found in {0}.", thisContext);
            FindDuplicates(
                virtualMachine.ConfigSets.Safe().Where(c => !String.IsNullOrEmpty(c)),
                dup => Error("ConfigSet '{0}' in {1} is present multiple times.", dup, thisContext));

            //DeploymentGroup is optional
            //virtualMachine.DeploymentGroup;
        }

        private void Validate(WindowsConfigSet windowsConfig, string context)
        {
            if (windowsConfig == null) throw new ArgumentNullException("windowsConfig");
            if (context == null) throw new ArgumentNullException("context");

            //LocalAdmin credentials are required
            ErrorIf(String.IsNullOrEmpty(windowsConfig.LocalAdminCredentialId),
                    "Missing Local Admin Credential for {0}.", context);

            //ComputerName is optional. Brewmaster will default to VM.Name; Azure generates psuedo-random name.
            if (windowsConfig.ComputerName != null)
            {
                Try(() => ValidationHelpers.ValidateWindowsComputerName(windowsConfig.ComputerName, "Computer Name"),
                    ex => Error("Invalid Windows Computer Name for {0}: {1}", context, ex.Message));
            }

            //DomainSettings are optional.
            var domainSettings = windowsConfig.DomainJoinSettings;
            if (domainSettings != null)
            {
                //DoaminName is required
                Try(() => ValidationHelpers.ValidateDomainName(domainSettings.DomainToJoin, "Domain to join"),
                    ex => Error("Invalid Domain Name for {0}: {1}", context, ex.Message));

                //Domain Join credential is required
                ErrorIf(String.IsNullOrEmpty(domainSettings.CredentialId),
                        "Missing Domain-Join credential for {0}.", context);

                //MachineOU is optional
                if (domainSettings.MachineOU != null)
                {
                    Try(() => ValidationHelpers.ValidateLdapOUName(domainSettings.MachineOU, "Machine OU"),
                        ex => Error("Invalid Machine OU for {0}': {1}", context, ex.Message));
                }
            }
        }

        private void Validate(OsVirtualDisk disk, string context)
        {
            if (disk == null) throw new ArgumentNullException("disk");
            if (context == null) throw new ArgumentNullException("context");

            var thisContext = "OS Disk in " + context;

            if (!String.IsNullOrEmpty(disk.DiskName))
            {
                //Existing Disk.
                Try(() => ValidationHelpers.ValidateDiskName(disk.DiskName, disk.GetType().Name),
                    ex => Error("Invalid DiskName for {0}: {1}", thisContext, ex.Message));

                //OsType is optional
                //OsImageName is optional.
            }
            else //New Disk
            {
                //OsType is Required
                ErrorIf(String.IsNullOrEmpty(disk.OsType),
                        "Must specify OS Type for new {0}.", thisContext);

                //OsImageName is Required.
                ErrorIf(String.IsNullOrEmpty(disk.OsImageName),
                        "Must specify OS Image for new {0}.", thisContext);
            }

            //MediaLink is optional. Brewmaster will autogenerate if required.
            if (disk.MediaLink != null)
            {
                ErrorIf(!Uri.IsWellFormedUriString(disk.MediaLink.ToString(), UriKind.Absolute),
                        "Malformed MediaLink URI for {0}.", thisContext);
            }

            //Make sure OsType (if given) is supported.
            if (disk.OsType != null)
            {
                ErrorIf(!disk.OsType.EqualNoCase("Windows"),
                        "Unsupported OS specified for {0}. Only Windows OS is currently supported.",
                        thisContext);
            }
        }

        private void Validate(VmEndpoint[] endpoints, string context)
        {
            if (endpoints == null) throw new ArgumentNullException("endpoints");
            if (context == null) throw new ArgumentNullException("context");

            foreach (var endpoint in endpoints)
            {
                var thisContext = new Lazy<string>(
                    () => String.Format("Endpoint '{0}' in {1}", endpoint.Name, context), false);

                //Name is required
                Try(() => ValidationHelpers.ValidateEndpointName(endpoint.Name, endpoint.GetType().Name),
                    ex => Error("Invalid Name for '{0}': {1}", thisContext.Value, ex.Message));

                //Public Port is optional. Brewmaster will assign value if needed.
                ErrorIf(endpoint.Port < 1 || endpoint.Port > 65535,
                        "PublicPort value out of range for {0}. Must be between 1 and 65535.", thisContext.Value);

                //Local Port is optional. Brewmaster will assign value if needed.
                ErrorIf(endpoint.LocalPort < 1 || endpoint.LocalPort > 65535,
                        "LocalPort  value out of range for {0}. Must be between 1 and 65535.", thisContext.Value);

                //Protocol is required
                ErrorIf(
                    endpoint.Protocol == null ||
                    !ValidationHelpers.AzureEndpointProtocolOptions.Contains(endpoint.Protocol,
                                                                      StringComparer.OrdinalIgnoreCase),
                    "Invalid Protocol for {0}. Must be {1}.", thisContext.Value,
                    ValidationHelpers.ConjoinWords(ValidationHelpers.AzureEndpointProtocolOptions, "or"));

                if (endpoint.EnableDirectServerReturn)
                {
                    ErrorIf(
                        endpoint.Port == null || endpoint.LocalPort == null || endpoint.Port != endpoint.LocalPort,
                        "EnableDirectServerReturn requires the LocalPort and PublicPort to be identical for {0}.",
                        thisContext.Value);
                }

                //LoadBalancerProbe is optional
                var loadBalancerProbe = endpoint.LoadBalancerProbe;
                if (loadBalancerProbe != null)
                {
                    //loadBalancerProbe.Name is required
                    Try(() => ValidationHelpers.ValidateEndpointName(loadBalancerProbe.Name, "Load Balancer"),
                        ex => Error("Invalid LoadBalancerProbe Name for '{0}': {1}", thisContext.Value, ex.Message));

                    //loadBalancerProbe.Protocol is required
                    ErrorIf(
                        loadBalancerProbe.Protocol == null ||
                        !ValidationHelpers.AzureEndpointProbeProtocolOptions.Contains(loadBalancerProbe.Protocol,
                                                                               StringComparer.OrdinalIgnoreCase),
                        "Invalid LoadBalancerProbe Protocol for {0}. Must be {1}.", thisContext.Value,
                        ValidationHelpers.ConjoinWords(ValidationHelpers.AzureEndpointProbeProtocolOptions, "or"));

                    //loadBalancerProbe.Path is optional
                    if (loadBalancerProbe.Path != null)
                    {
                        if (loadBalancerProbe.Protocol.EqualNoCase("TCP"))
                        {
                            Error("Invalid LoadBalancerProbe Path in {0}. TCP probes cannot specify a Path.",
                                  thisContext.Value);
                        }
                        else
                        {
                            ErrorIf(!Uri.IsWellFormedUriString(loadBalancerProbe.Path, UriKind.Relative),
                                    "Malformed LoadBalancerProbe Path in {0}. Path must be a relative URL.",
                                    thisContext.Value);
                        }
                    }
                    else
                    {
                        WarningIf(loadBalancerProbe.Protocol.EqualNoCase("HTTP"),
                                  "Empty Path in HTTP LoadBalancerProbe in {0}.",
                                  thisContext.Value);
                    }

                    //loadBalancerProbe.Port is optional
                    ErrorIf(loadBalancerProbe.Port < 1 || loadBalancerProbe.Port > 65535,
                            "LoadBalancerProbe Port value out of range for {0}. Must be between 1 and 65535.",
                            thisContext.Value);

                    //loadBalancerProbe.Interval is optional
                    ErrorIf(
                        loadBalancerProbe.IntervalInSeconds < ValidationHelpers.AzureEndpointProbeMinIntervalInSeconds,
                        "LoadBalancerProbe Interval value out of range for {0}. Must be greater than 5 seconds.",
                        thisContext.Value);

                    //loadBalancerProbe.Timeout is optional
                    ErrorIf(
                        loadBalancerProbe.TimeoutInSeconds < ValidationHelpers.AzureEndpointProbeMinTimeoutInSeconds,
                        "LoadBalancerProbe Timeout value out of range for {0}. Must be greater than 11 seconds.",
                        thisContext.Value);
                }

                //Rules are optional
                foreach (var rule in endpoint.Rules.Safe())
                {
                    //Order is required
                    ErrorIf(rule.Order < 0 || rule.Order > 65535,
                            "Order out of range for rule {0} in {1}. Must be between 0 and 65535.",
                            rule.Order, thisContext.Value);

                    //Action is required
                    ErrorIf(
                        rule.Action == null ||
                        !ValidationHelpers.AzureEndpointACLActions.Contains(rule.Action, StringComparer.OrdinalIgnoreCase),
                        "Invalid Action value for rule {0} in {1}. Must be {2}.",
                        rule.Order, thisContext.Value, ValidationHelpers.ConjoinWords(ValidationHelpers.AzureEndpointACLActions, "or"));

                    //Remote subnet is required
                    if (!ErrorIf(rule.RemoteSubnet == null,
                                 "Missing Remote Subnet CIDR for rule {0} in {1}.",
                                 rule.Order, thisContext.Value))
                    {
                        Try(() => CIDRAddressSpace.Parse(rule.RemoteSubnet),
                            e => Error("Error parsing Remote Subnet CIDR for rule {0} in {1}: {2}",
                                       rule.Order, thisContext.Value, e.Message));
                    }

                    //Description is optional
                    ErrorIf(rule.Description != null && rule.Description.Length > 256,
                            "Description too long for rule {0} in {1}. Must be 256 characters or less.",
                            rule.Order, thisContext.Value);
                }

                //Rule.Order must be unique
                FindDuplicates(
                    endpoint.Rules.Safe(), r => r.Order, EqualityComparer<int>.Default,
                    dup => Error("Rule {0} in {1} is present multiple times.", dup.Order, thisContext.Value));
            }

            //Endpoints must be unique
            FindDuplicates(
                endpoints.Where(o => o != null && !String.IsNullOrEmpty(o.Name)),
                dup => Error("Endpoint '{0}' in {1} is present multiple times.", dup.Name, context));
        }

        private void Validate(DataVirtualDisk[] dataDisks, string context)
        {
            if (dataDisks == null) throw new ArgumentNullException("dataDisks");
            if (context == null) throw new ArgumentNullException("context");

            foreach (var disk in dataDisks)
            {
                var thisContext = new Lazy<string>(
                    () => String.Format("Data disk '{0}' in {1}", disk.DiskId, context), false);

                //Id is required
                Try(() => ValidationHelpers.ValidateIdentifier(disk.DiskId, disk.GetType().Name),
                    ex => Error("Invalid Disk Id for '{0}': {1}", thisContext.Value, ex.Message));

                if (disk.DiskName != null)
                {
                    //Existing Disk.
                    //DiskName is required
                    Try(() => ValidationHelpers.ValidateDiskName(disk.DiskName, "Disk Name"),
                        ex => Error("Invalid DiskName for {0}: {1}", thisContext.Value, ex.Message));
                }
                else //New disk
                {
                    //Disk size is required
                    ErrorIf(disk.LogicalSizeInGB == null,
                            "Must specify Logical Size of new {0}.", thisContext.Value);
                }

                //Validate Disk Size if given.
                if (disk.LogicalSizeInGB != null)
                {
                    ErrorIf(disk.LogicalSizeInGB < ValidationHelpers.MinAzureDataSizeSizeInGB ||
                            disk.LogicalSizeInGB > ValidationHelpers.MaxAzureDataSizeSizeInGB,
                            "Logical Size is out of range ({1}-{2}) in {0}.",
                            thisContext.Value, ValidationHelpers.MinAzureDataSizeSizeInGB,
                            ValidationHelpers.MaxAzureDataSizeSizeInGB);
                }

                //MediaLink is optional. Brewmaster will autogenerate if required.
                if (disk.MediaLink != null)
                {
                    ErrorIf(!Uri.IsWellFormedUriString(disk.MediaLink.ToString(), UriKind.Absolute),
                            "Malformed MediaLink URI for {0}.", thisContext.Value);
                }

                //LUN is optional. Brewmaster will assign a value if required.
                if (disk.Lun != null)
                {
                    ErrorIf(disk.Lun < ValidationHelpers.MinAzureDataDiskLun ||
                            disk.Lun > ValidationHelpers.MaxAzureDataDiskLun,
                            "LUN is out of range ({1}-{2}) in {0}.",
                            thisContext.Value, ValidationHelpers.MinAzureDataDiskLun, ValidationHelpers.MaxAzureDataDiskLun);
                }

                //HostCaching is optional. Default is 'None'.
                if (disk.HostCaching != null)
                {
                    ErrorIf(
                        !ValidationHelpers.AzureDataDiskHostCachingOptions.Contains(disk.HostCaching,
                                                                                    StringComparer.OrdinalIgnoreCase),
                        "Invalid Host Caching value in {0}.", thisContext.Value);
                }
            }

            //Check for duplicate LUNs
            FindDuplicates(
                dataDisks.Where(d => !String.IsNullOrEmpty(d.DiskId) && d.Lun.HasValue), d => d.Lun,
                EqualityComparer<int?>.Default,
                dup => Error("LUN of Data disk '{0}' in {1} conflicts with another disk.", dup.DiskId, context));

            //Check for duplicate Disks
            FindDuplicates(
                dataDisks.Where(d => !String.IsNullOrEmpty(d.DiskId)), d => d.DiskId,
                StringComparer.OrdinalIgnoreCase,
                dup => Error("Data disk '{0}' in {1} is present multiple times.", dup.DiskId, context));
        }

        private void Validate(DeploymentGroup[] deploymentGroups)
        {
            if (deploymentGroups == null) throw new ArgumentNullException("deploymentGroups");

            foreach (var deploymentGroup in deploymentGroups)
            {
                Try(() => ValidationHelpers.ValidateIdentifier(deploymentGroup.Name, deploymentGroup.GetType().Name),
                    e => Error("Invalid Deployment Group name '{0}': {1}",
                               deploymentGroup.GetType().Name, e.Message));
            }

            FindDuplicates(
                deploymentGroups.Where(o => o != null && !String.IsNullOrEmpty(o.Name)),
                dup => Error("Deployment Group '{0}' is present multiple times.", dup.Name));
        }

        private void Validate(Credential[] credentials)
        {
            if (credentials == null) throw new ArgumentNullException("credentials");

            foreach (var credential in credentials)
            {
                Try(() => ValidationHelpers.ValidateIdentifier(credential.Name, credential.GetType().Name),
                    e => Error("Invalid Id for Credential '{0}': {1}", credential.GetType().Name, e.Message));

                if (!String.IsNullOrEmpty(credential.UserName))
                {
                    Try(() => ValidationHelpers.ValidateWindowsUserName(credential.UserName, "Username"),
                        ex => Error(
                            "Error validating Username for Credential '{0}': {1}", credential.Name, ex.Message));
                }

                if (String.IsNullOrEmpty(credential.Password))
                {
                    if (String.IsNullOrEmpty(credential.UserName))
                    {
                        Error("Must specify a username and/or password for Credential '{0}'.",
                              credential.Name);
                    }
                    else
                    {
                        Warning("Empty password for Credential '{0}'.", credential.Name);
                    }
                }
                else
                {
                    Try(() => ValidationHelpers.ValidatePasswordComplexity(credential.Password, "Password"),
                        ex => Error(
                            "Error validating Password for Credential '{0}': {1}", credential.Name, ex.Message));
                }
            }

            FindDuplicates(
                credentials.Where(c => !String.IsNullOrEmpty(c.Name)), c => c.Name,
                StringComparer.OrdinalIgnoreCase,
                dup => Error("Credential '{0}' is present multiple times.", dup.Name));
        }

        //public void Validate(Certificate[] certificates)
        //{
        //    if (certificates == null) throw new ArgumentNullException("certificates");

        //    foreach (var certificate in certificates)
        //    {
        //        Try(() => ValidationHelpers.ValidateIdentifier(certificate.Name, certificate.GetType().Name),
        //            e => Error("Invalid Certificate Id '{0}': {1}",
        //                       certificate.GetType().Name, e.Message));

        //        //TODO: Validate other stuff
        //    }

        //    FindDuplicates(
        //        certificates.Where(c => !String.IsNullOrEmpty(c.Name)), c => c.Name,
        //        StringComparer.OrdinalIgnoreCase,
        //        dup => Error("Certificate '{0}' is present multiple times.", dup.Name));
        //}

        private void Validate(ConfigSet[] configSets)
        {
            if (configSets == null) throw new ArgumentNullException("configSets");

            foreach (var configSet in configSets)
            {
                var thisContext = String.Format(CultureInfo.InvariantCulture, "ConfigSet Id '{0}'", configSet.Name);

                Try(() => ValidationHelpers.ValidateIdentifier(configSet.Name, configSet.GetType().Name),
                    e => Error("Invalid ConfigSet Id '{0}': {1}",
                               configSet.GetType().Name, e.Message));

                if (configSet.Endpoints != null)
                {
                    Validate(configSet.Endpoints, thisContext);
                }

                //ErrorIf(configSet.CertificateIds.Safe().Any(String.IsNullOrEmpty),
                //        "Empty Certificate reference found in {0}.", thisContext);

                //FindDuplicates(
                //    configSet.CertificateIds.Safe().Where(c => !String.IsNullOrEmpty(c)),
                //    dup => Error("Certificate '{0}' in {1} is present multiple times.", dup, thisContext));

                ErrorIf(configSet.ConfigurationIds.Any(String.IsNullOrEmpty),
                        "Empty Configuration reference found in {0}.", thisContext);

                FindDuplicates(
                    configSet.ConfigurationIds.Where(c => !String.IsNullOrEmpty(c)),
                    dup => Error("Configuration '{0}' in {1} is present multiple times.", dup, thisContext));
            }

            FindDuplicates(
                configSets.Where(c => !String.IsNullOrEmpty(c.Name)), c => c.Name,
                StringComparer.OrdinalIgnoreCase,
                dup => Error("ConfigSet '{0}' is present multiple times.", dup.Name));
        }

        private bool Try(Action action, Action<Exception> exceptionHandler)
        {
            try
            {
                action();
                return true;
            }
            catch (Exception e)
            {
                exceptionHandler(e);
                return false;
            }
        }

        private static bool TryParseDouble(string value, out double result)
        {
            return double.TryParse(value,
                                   NumberStyles.AllowLeadingSign |
                                   NumberStyles.AllowDecimalPoint |
                                   NumberStyles.AllowExponent,
                                   CultureInfo.InvariantCulture,
                                   out result);
        }

        private static bool TryParseInteger(string value, out long result)
        {
            return long.TryParse(value,
                                 NumberStyles.AllowLeadingSign,
                                 CultureInfo.InvariantCulture,
                                 out result);
        }

        private static bool TryParseBoolean(string value, out bool result)
        {
            if (bool.TryParse(value, out result)) return true;
            long integer;
            if (TryParseInteger(value, out integer) /*&& (integer == 0 || integer == 1)*/)
            {
                result = (integer != 0);
                return true;
            }
            return false;
        }

        private static bool TryParseDate(string value, out DateTime result)
        {
            return DateTime.TryParse(value, 
                                     CultureInfo.InvariantCulture,
                                     DateTimeStyles.AssumeUniversal,
                                     out result);
        }

        private static string[] ParseCsvLine(string defaultValues)
        {
            using (var reader = new System.IO.StringReader(defaultValues))
            {
                var csvParser = new Microsoft.VisualBasic.FileIO.TextFieldParser(reader)
                    {
                        TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited,
                        Delimiters = new[] {","},
                        HasFieldsEnclosedInQuotes = true,
                        TrimWhiteSpace = false
                    };
                return csvParser.ReadFields() ?? new string[0];
            }
        }

        private void FindDuplicates<TItem>(IEnumerable<TItem> collection, Action<TItem> duplicateAction)
        {
            if (typeof (TItem) == typeof (string))
                FindDuplicates(collection, i => i as string, StringComparer.OrdinalIgnoreCase, duplicateAction);
            else
                FindDuplicates<TItem, string>(collection, i => ((dynamic) i).Name, StringComparer.OrdinalIgnoreCase,
                                              duplicateAction);
        }

        private void FindDuplicates<TItem, TKey>(IEnumerable<TItem> collection, Func<TItem, TKey> keySelector,
                                                 IEqualityComparer<TKey> comparer, Action<TItem> duplicateAction)
        {
            if (collection == null) throw new ArgumentNullException("collection");
            if (keySelector == null) throw new ArgumentNullException("keySelector");
            if (comparer == null) throw new ArgumentNullException("comparer");
            if (duplicateAction == null) throw new ArgumentNullException("duplicateAction");

            var dups = new Dictionary<TKey, int>(comparer);
            foreach (var item in collection)
            {
                if ((object) item == null) continue;

                var key = keySelector(item);
                if (!dups.ContainsKey(key))
                {
                    dups.Add(key, 1);
                }
                else if (++dups[key] == 2)
                {
                    //first duplicate
                    duplicateAction(item);
                }
            }
        }

        [StringFormatMethod("message")]
        private void Error(string message, params object[] formatArgs)
        {
            _resultCollector.AddFailure(formatArgs != null ? String.Format(CultureInfo.InvariantCulture, message, formatArgs) : message);
        }

        [StringFormatMethod("message")]
        private bool ErrorIf(bool condition, string message, params object[] formatArgs)
        {
            if (condition)
            {
                Error(message, formatArgs);
            }
            return condition;
        }

        [StringFormatMethod("message")]
        private void Warning(string message, params object[] formatArgs)
        {
            _resultCollector.AddWarning(formatArgs != null ? String.Format(CultureInfo.InvariantCulture, message, formatArgs) : message);
        }

        [StringFormatMethod("message")]
        private bool WarningIf(bool condition, string message, params object[] formatArgs)
        {
            if (condition)
            {
                Warning(message, formatArgs);
            }
            return condition;
        }
        
    }
}
