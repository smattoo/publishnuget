namespace Brewmaster.TemplateSDK.Contracts.Validation
{
    public interface IValidationResults
    {
        void AddFailure(string message);
        void AddWarning(string message);
    }
}