using System.Collections.Generic;
using Brewmaster.TemplateSDK.Contracts.Models;

namespace Brewmaster.TemplateSDK.Contracts.Validation
{
    public interface ITemplateValidator
    {
        ValidationResults Validate(string templateJson, IDictionary<string, string> @params = null);
        IDictionary<string, string> GenerateParamsFromTemplate(string templateJson);
    }
}