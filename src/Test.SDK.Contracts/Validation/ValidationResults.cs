using System.Collections.Generic;
using System.Linq;

namespace Brewmaster.TemplateSDK.Contracts.Validation
{
    public class ValidationResults
    {
        public ValidationResults()
        {
            Messages = new List<ValidationMessage>();
        }

        public bool IsValid
        {
            get { return Messages.All(m => m.Type != MessageType.Error); }
        }

        public List<ValidationMessage> Messages { get; set; }

        public void AddFailure(string message)
        {
            Messages.Add(new ValidationMessage {Message = message, Type = MessageType.Error});
        }

        public void AddWarning(string message)
        {
            Messages.Add(new ValidationMessage {Message = message, Type = MessageType.Warning});
        }
    }

    public class ValidationMessage
    {
        public string Message { get; set; }
        public MessageType Type { get; set; }
    }

    public enum MessageType
    {
        Error,
        Warning,
    }
}