using System;
using Brewmaster.TemplateSDK.Contracts.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Brewmaster.TemplateSDK.Contracts.Serialization
{
    public class ResourceJsonConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            JToken.FromObject(value).WriteTo(writer);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
                                        JsonSerializer serializer)
        {
            var jObject = serializer.Deserialize<JObject>(reader);
            //Trace.WriteLine(jObject.ToString());

            var type = jObject.Value<string>("Type");
            if (type == null)
                throw new JsonSerializationException("Resource missing Type property.");

            switch (type.ToLowerInvariant())
            {
                case "script":
                    return jObject.ToObject<ScriptResource>();

                default:
                    return jObject.ToObject<GenericResource>();
            }
        }

        public override bool CanConvert(Type objectType)
        {
            return typeof (GenericResource).IsAssignableFrom(objectType);
        }
    }
}