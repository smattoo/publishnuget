using System.Runtime.Serialization.Formatters;
using Brewmaster.TemplateSDK.Contracts.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Brewmaster.TemplateSDK.Contracts.Serialization
{
    public static class TemplateSerialization
    {
        public static string ToJson<T>(this T template, bool pretty = true)
        {
            var serializerSettings = JsonSerializerSettings;
            var json = JsonConvert.SerializeObject(template, pretty ? Formatting.Indented : Formatting.None, serializerSettings);
            return json;
        }

        public static BrewmasterTemplate ToBrewmasterTemplate(this string json)
        {
            var serializerSettings = JsonSerializerSettings;
            var template = JsonConvert.DeserializeObject<BrewmasterTemplate>(json, serializerSettings);
            return template;
        }

        public static JsonSerializerSettings JsonSerializerSettings
        {
            get
            {
                var serializerSettings = new JsonSerializerSettings
                    {
                        ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor,
                        DateFormatHandling = DateFormatHandling.IsoDateFormat,
                        DateTimeZoneHandling = DateTimeZoneHandling.RoundtripKind,
                        MissingMemberHandling = MissingMemberHandling.Error,
                        TypeNameAssemblyFormat = FormatterAssemblyStyle.Simple,
                        TypeNameHandling = TypeNameHandling.Auto,
                        NullValueHandling = NullValueHandling.Ignore,
                        Converters = new JsonConverter[] {new StringEnumConverter(), new ResourceJsonConverter()},
                    };
                return serializerSettings;
            }
        }
    }
}