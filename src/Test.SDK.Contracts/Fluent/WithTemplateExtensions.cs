using System;
using System.Linq;
using Brewmaster.TemplateSDK.Contracts.Models;
using Brewmaster.TemplateSDK.Contracts.Serialization;
using Brewmaster.TemplateSDK.Contracts.Validation;
using JetBrains.Annotations;
using System.IO;

namespace Brewmaster.TemplateSDK.Contracts.Fluent
{
    public static class WithTemplateExtensions
    {
        public static BrewmasterTemplate CreateTemplate(string name, string description)
        {
            if (name == null) throw new ArgumentNullException("name");

            return new BrewmasterTemplate
                {
                    Name = name,
                    Description = description,
                };
        }

        public static void Save(this BrewmasterTemplate template, string folder)
        {
            if (folder == null) throw new ArgumentNullException("folder");

            folder = string.Join("_", folder.Split(Path.GetInvalidPathChars()));

            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }

            var filesFolder = Path.Combine(folder, "Files");
            if (!Directory.Exists(filesFolder))
            {
                Directory.CreateDirectory(filesFolder);
            }

            var powershellFolder = Path.Combine(folder, "Files", "PowerShell");
            if (!Directory.Exists(powershellFolder))
            {
                Directory.CreateDirectory(powershellFolder);
            }

            var readmeFile = Path.Combine(folder, "README.md");
            if (!File.Exists(readmeFile))
            {
                File.WriteAllText(readmeFile, string.Format("Brewmaster Template : {0}", template.Name));
            }

            var templateFile = Path.Combine(folder, "Template.json");
            File.WriteAllText(templateFile, template.ToJson(true));

        }

        internal static T[] InsertOrUpdateNamedItem<T>([CanBeNull] this T[] collection, T item, Func<T, string> nameSelector)
        {
            if (collection == null)
            {
                return new[] {item};
            }

            var key = nameSelector(item);
            var idx = Array.FindIndex(collection, i => key.EqualNoCase(nameSelector(i)));
            if (idx != -1)
            {
                //update in-place
                collection[idx] = item;
                return collection;
            }

            //append
            return collection.Concat(new[] { item }).ToArray();
        }

        public static BrewmasterTemplate WithDeploymentGroup(this BrewmasterTemplate template, string name,
                                                             string description = null)
        {
            if (name == null) throw new ArgumentNullException("name");

            var deploymentGroup = new DeploymentGroup
                {
                    Name = name,
                    Description = description,
                };

            var clone = template.Clone();
            clone.DeploymentGroups = InsertOrUpdateNamedItem(clone.DeploymentGroups, deploymentGroup, s => s.Name);
            return clone;
        }

        public static BrewmasterTemplate WithCredential(this BrewmasterTemplate template, string id, string username,
                                                        string password = null)
        {
            if (id == null) throw new ArgumentNullException("id");
            if (username == null) throw new ArgumentNullException("username");
            if (username == "" && String.IsNullOrEmpty(password)) throw new ArgumentException("Username and password cannot both be empty.", "password");

            var credential = new Credential
                {
                    Name = id,
                    UserName = username,
                    Password = password ?? ""
                };

            var clone = template.Clone();
            clone.Credentials = clone.Credentials.InsertOrUpdateNamedItem(credential, c => c.Name);
            return clone;
        }

        //public static BrewmasterTemplate WithCertificate(this BrewmasterTemplate template, string id, string pfxFileName,
        //                                                 string password = null)
        //{
        //    if (id == null) throw new ArgumentNullException("id");
        //    if (String.IsNullOrWhiteSpace(pfxFileName)) throw new ArgumentNullException("pfxFileName");

        //    var cert = new Certificate
        //        {
        //            Name = id,
        //            FileName = pfxFileName,
        //            Password = password ?? "",
        //        };

        //    var clone = template.Clone();
        //    clone.Certificates = clone.Certificates.InsertOrUpdateNamedItem(cert, c => c.Name);
        //    return clone;
        //}
    }
}
