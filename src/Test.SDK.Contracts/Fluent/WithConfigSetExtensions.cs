using System;
using Brewmaster.TemplateSDK.Contracts.Models;
using Brewmaster.TemplateSDK.Contracts.Validation;
using JetBrains.Annotations;

namespace Brewmaster.TemplateSDK.Contracts.Fluent
{
    public static class WithConfigSetExtensions
    {
        public static BrewmasterTemplate WithConfigSet(this BrewmasterTemplate template, ConfigSet configSet)
        {
            if (configSet == null) throw new ArgumentNullException("configSet");

            var clone = template.Clone();
            clone.ConfigSets = clone.ConfigSets.InsertOrUpdateNamedItem(configSet, r => r.Name);
            return clone;
        }

        public static BrewmasterTemplate WithConfigSet(this BrewmasterTemplate template, string id, string description,
                                                  Func<ConfigSet, ConfigSet> builder = null)
        {
            if (id == null) throw new ArgumentNullException("id");

            var role = new ConfigSet
                {
                    Name = id,
                    Description = description
                };

            if (builder != null)
            {
                role = builder(role);
            }
            return template.WithConfigSet(role);
        }

        public static ConfigSet WithEndpoint(this ConfigSet role, VmEndpoint endpoint)
        {
            if (endpoint == null) throw new ArgumentNullException("endpoint");

            var clone = role.Clone();
            clone.Endpoints = clone.Endpoints.InsertOrUpdateNamedItem(endpoint, e => e.Name);
            return clone;
        }

        public static ConfigSet WithEndpoint(this ConfigSet role, string name, int? localPort, int? publicPort,
                                             EndpointLoadBalancerProbe loadBalancerProbe = null,
                                             string protocol = "tcp")
        {
            if (name == null) throw new ArgumentNullException("name");

            var endpoint = new VmEndpoint
                {
                    Name = name,
                    LocalPort = localPort,
                    Port = publicPort,
                    Protocol = protocol,
                    LoadBalancerProbe = loadBalancerProbe.Clone(),
                };

            return role.WithEndpoint(endpoint);
        }

        //public static ConfigSet UsingCertificate(this ConfigSet vm, string certificateId)
        //{
        //    if (certificateId == null) throw new ArgumentNullException("certificateId");

        //    var clone = vm.Clone();
        //    clone.CertificateIds = clone.CertificateIds.InsertOrUpdateNamedItem(certificateId, c => c);
        //    return clone;
        //}

        public static ConfigSet UsingConfiguration(this ConfigSet vm, params string[] configurationId)
        {
            if (configurationId == null) throw new ArgumentNullException("configurationId");

            var clone = vm.Clone();
            foreach (var id in configurationId)
            {
                clone.ConfigurationIds = clone.ConfigurationIds.InsertOrUpdateNamedItem(id, c => c);
            }
            return clone;
        }
    }
}