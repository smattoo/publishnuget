using System;
using System.Linq;
using System.Net;
using Brewmaster.TemplateSDK.Contracts.Models;
using Brewmaster.TemplateSDK.Contracts.Validation;
using JetBrains.Annotations;

namespace Brewmaster.TemplateSDK.Contracts.Fluent
{
    public static class WithAzureExtensions
    {
        public static BrewmasterTemplate WithAffinityGroup(this BrewmasterTemplate template, string name, string region,
                                                           string label = null, string description = null,
                                                           Func<AffinityGroup, AffinityGroup> builder = null)
        {
            if (name == null) throw new ArgumentNullException("name");

            var affinityGroup = new AffinityGroup
                {
                    Name = name,
                    Region = region,
                    Description = description,
                    Label = label //default is Name
                };

            if (builder != null)
            {
                affinityGroup = builder(affinityGroup);
            }
            return template.WithAffinityGroup(affinityGroup);
        }

        public static BrewmasterTemplate WithAffinityGroup(this BrewmasterTemplate template, AffinityGroup affinityGroup)
        {
            if (affinityGroup == null) throw new ArgumentNullException("affinityGroup");

            var clone = template.Clone();
            clone.AffinityGroup = affinityGroup;
            return clone;
        }

        public static BrewmasterTemplate WithStorageAccount(this BrewmasterTemplate template, StorageAccount account)
        {
            if (account == null) throw new ArgumentNullException("account");

            var clone = template.Clone();
            clone.StorageAccounts = clone.StorageAccounts.InsertOrUpdateNamedItem(account, a => a.Name);
            return clone;
        }

        public static BrewmasterTemplate WithStorageAccount(this BrewmasterTemplate template, string name,
                                                            string description = null,
                                                            bool disableGeoReplication = false)
        {
            if (name == null) throw new ArgumentNullException("name");
            if (template.AffinityGroup == null)
                throw new InvalidOperationException("Must specify an affinity group first.");

            var account = new StorageAccount
                {
                    Name = name,
                    AffinityGroup = template.AffinityGroup.Name,
                    Region = template.AffinityGroup.Region, //ignored if AffinityGroup != null
                    DisableGeoReplication = disableGeoReplication,
                    Description = description,
                    Label = null //default is Name
                };

            return WithStorageAccount(template, account);
        }

        public static BrewmasterTemplate WithCloudService(this BrewmasterTemplate template, CloudService service)
        {
            if (service == null) throw new ArgumentNullException("service");

            var clone = template.Clone();
            clone.CloudServices = clone.CloudServices.InsertOrUpdateNamedItem(service, s => s.Name);
            return clone;
        }

        public static BrewmasterTemplate WithCloudService(this BrewmasterTemplate template, string name,
                                                          string description = null,
                                                          Func<CloudService, CloudService> builder = null)
        {
            if (name == null) throw new ArgumentNullException("name");
            if (template.AffinityGroup == null)
                throw new InvalidOperationException("Must specify an affinity group first.");

            var cs = new CloudService
                {
                    Name = name,
                    AffinityGroup = template.AffinityGroup.Name,
                    Region = template.AffinityGroup.Region, //ignored if AffinityGroup != null
                    Description = description,
                    Label = null //default is Name
                };

            if (builder != null)
            {
                cs = builder(cs);
            }
            return WithCloudService(template, cs);
        }

        public static CloudService WithDeployment(this CloudService cloudService, VmDeployment deployment)
        {
            if (deployment == null) throw new ArgumentNullException("deployment");

            var clone = cloudService.Clone();
            clone.Deployment = deployment;
            return clone;
        }

        public static CloudService WithDeployment(this CloudService cloudService, string deploymentName = null,
                                                  Func<VmDeployment, VmDeployment> builder = null)
        {
            var dep = new VmDeployment
                {
                    Name = deploymentName,
                    Label = null, //default is Name
                    VirtualNetwork = null,
                    DiskStorageAccount = null,
                    DeploymentId = null //auto-generated by Azure
                };

            if (builder != null)
            {
                dep = builder(dep);
            }
            return WithDeployment(cloudService, dep);
        }

        public static VmDeployment UsingVirtualNetwork(this VmDeployment deployment, string virtualNetwork)
        {
            if (virtualNetwork == null) throw new ArgumentNullException("virtualNetwork");

            var clone = deployment.Clone();
            clone.VirtualNetwork = virtualNetwork; //required when creating new disks
            return clone;
        }

        public static VmDeployment UsingDNSServer(this VmDeployment deployment, string dnsName, string ipAddress)
        {
            if (dnsName == null) throw new ArgumentNullException("dnsName");

            var server = new DnsServer
                {
                    Name = dnsName,
                    IPAddress = ipAddress,
                };

            var clone = deployment.Clone();
            clone.DnsServers = clone.DnsServers.InsertOrUpdateNamedItem(server, s => s.Name);
            return clone;
        }

        public static VmDeployment UsingDefaultDiskStorage(this VmDeployment deployment, string storageAccount)
        {
            if (storageAccount == null) throw new ArgumentNullException("storageAccount");

            var clone = deployment.Clone();
            clone.DiskStorageAccount = storageAccount; //required when creating new disks
            return clone;
        }

        public static VmDeployment WithVirtualMachine(this VmDeployment deployment, VirtualMachine machine)
        {
            if (machine == null) throw new ArgumentNullException("machine");

            var clone = deployment.Clone();
            clone.VirtualMachines = clone.VirtualMachines.InsertOrUpdateNamedItem(machine, a => a.Name);
            return clone;
        }

        public static VmDeployment WithVirtualMachine(this VmDeployment deployment, string name, string roleSize,
                                                      string availabilitySet = null,
                                                      Func<VirtualMachine, VirtualMachine> builder = null)
        {
            if (name == null) throw new ArgumentNullException("name");

            var vm = new VirtualMachine
                {
                    Name = name,
                    RoleSize = roleSize,
                    AvailabilitySet = availabilitySet //recommended but not required
                };

            if (builder != null)
            {
                vm = builder(vm);
            }
            return WithVirtualMachine(deployment, vm);
        }

        public static VirtualMachine UsingSubnet(this VirtualMachine vm, params string[] subnets)
        {
            if (subnets == null) throw new ArgumentNullException("subnets");

            var clone = vm.Clone();
            foreach (var subnet in subnets)
            {
                clone.Subnets = clone.Subnets.InsertOrUpdateNamedItem(subnet, s => s);
            }
            return clone;
        }

        public static VirtualMachine WithEndpoint(this VirtualMachine vm, string name, int? localPort, int? publicPort,
                                                  EndpointLoadBalancerProbe loadBalancerProbe = null,
                                                  string protocol = "tcp")
        {
            if (name == null) throw new ArgumentNullException("name");

            var endpoint = new VmEndpoint
                {
                    Name = name,
                    LocalPort = localPort, //auto-generated if null
                    Port = publicPort, //auto-generated if null
                    Protocol = protocol,
                    LoadBalancerProbe = loadBalancerProbe.Clone(),
                    EnableDirectServerReturn = false
                };

            return WithEndpoint(vm, endpoint);
        }

        public static VirtualMachine WithEndpoint(this VirtualMachine vm, VmEndpoint endpoint)
        {
            if (endpoint == null) throw new ArgumentNullException("endpoint");

            var clone = vm.Clone();
            clone.Endpoints = clone.Endpoints.InsertOrUpdateNamedItem(endpoint, e => e.Name);
            return clone;
        }

        public static VirtualMachine WithNewPlatformImage(this VirtualMachine vm,
                                                          string osImageWildcardFilter,
                                                          string osType, Uri mediaLink = null)
        {
            if (String.IsNullOrWhiteSpace(osImageWildcardFilter))
                throw new ArgumentNullException("osImageWildcardFilter");
            if (String.IsNullOrWhiteSpace(osType)) throw new ArgumentNullException("osType");

            var osVirtualDisk = new OsVirtualDisk
                {
                    OsType = osType,
                    OsImageName = osImageWildcardFilter,
                    MediaLink = mediaLink, //auto-generated by BM if null
                    DiskName = null //auto-generated by Azure
                };

            var clone = vm.Clone();
            clone.OsVirtualDisk = osVirtualDisk;
            return clone;
        }

        public static VirtualMachine WithExistingPlatformImage(this VirtualMachine vm, string diskName,
                                                               string osType)
        {
            if (diskName == null) throw new ArgumentNullException("diskName");

            var osVirtualDisk = new OsVirtualDisk
                {
                    OsType = osType,
                    DiskName = diskName,
                    OsImageName = null, //not needed when using an existing disk
                    MediaLink = null //not needed when using an existing disk
                };

            var clone = vm.Clone();
            clone.OsVirtualDisk = osVirtualDisk;
            return clone;
        }

        public static VirtualMachine WithNewDataDisk(this VirtualMachine vm, string diskId, int sizeInGb,
                                                     int? lun = null, string hostCaching = null, Uri mediaLink = null)
        {
            if (diskId == null) throw new ArgumentNullException("diskId");

            var disk = new DataVirtualDisk
                {
                    DiskId = diskId, //required
                    MediaLink = mediaLink, //auto-generated by BM if null
                    LogicalSizeInGB = sizeInGb, //required
                    Lun = lun,
                    HostCaching = hostCaching,
                    DiskName = null //will be auto-generated by Azure
                };

            var clone = vm.Clone();
            clone.DataVirtualDisks = clone.DataVirtualDisks.InsertOrUpdateNamedItem(disk, d => d.DiskId);
            return clone;
        }

        public static VirtualMachine WithStaticIPAddress(this VirtualMachine vm, string ipaddress)
        {
            if (String.IsNullOrEmpty(ipaddress)) throw new ArgumentNullException("ipaddress");

            var clone = vm.Clone();
            clone.StaticVNetIPAddress = ipaddress;
            return clone;
        }

        public static VirtualMachine WithExistingDataDisk(this VirtualMachine vm, string diskId, string diskName,
                                                          int? lun = null, string hostCaching = null)
        {
            if (diskId == null) throw new ArgumentNullException("diskId");
            if (diskName == null) throw new ArgumentNullException("diskName");

            var disk = new DataVirtualDisk
                {
                    DiskId = diskId, //required
                    DiskName = diskName, //required
                    Lun = lun,
                    HostCaching = hostCaching,
                    MediaLink = null, //not needed when using an existing disk
                    LogicalSizeInGB = null, //not needed when using an existing disk
                };

            var clone = vm.Clone();
            clone.DataVirtualDisks = clone.DataVirtualDisks.InsertOrUpdateNamedItem(disk, d => d.DiskId);
            return clone;
        }

        public static VirtualMachine WithWindowsConfigSet(this VirtualMachine vm, string adminCredentialId,
                                                          bool disableRdp = false)
        {
            if (adminCredentialId == null) throw new ArgumentNullException("adminCredentialId");

            var windowsConfigSet = new WindowsConfigSet
                {
                    ComputerName = null, //will default to vm.Name.ToUpper()
                    LocalAdminCredentialId = adminCredentialId,
                    DisableRdp = disableRdp,
                    DomainJoinSettings = null
                };

            var clone = vm.Clone();
            clone.WindowsConfigSet = windowsConfigSet;

            if (clone.OsVirtualDisk == null || 
                (clone.OsVirtualDisk.OsImageName == null && clone.OsVirtualDisk.DiskName == null))
            {
                clone = clone.WithNewPlatformImage("a699494373c04fc0bc8f2bb1389d6106__Windows-Server-2012-R2-*",
                                                   "Windows");
            }

            return clone;
        }

        public static VirtualMachine WithDomain(this VirtualMachine vm, string fullyQualifedDomainName,
                                                string domainJoinCredential, string machineOU = null)
        {
            if (fullyQualifedDomainName == null) throw new ArgumentNullException("fullyQualifedDomainName");
            if (domainJoinCredential == null) throw new ArgumentNullException("domainJoinCredential");

            if (vm.WindowsConfigSet == null)
                throw new InvalidOperationException("Must specify Windows Config Set first.");

            var vmDomainJoinSettings = new VmDomainJoinSettings
                {
                    DomainToJoin = fullyQualifedDomainName,
                    CredentialId = domainJoinCredential,
                    MachineOU = machineOU,
                };

            var clone = vm.Clone();
            clone.WindowsConfigSet.DomainJoinSettings = vmDomainJoinSettings;
            return clone;
        }

        public static VirtualMachine UsingConfigSet(this VirtualMachine vm, params string[] roleNames)
        {
            if (roleNames == null) throw new ArgumentNullException("roleNames");

            var clone = vm.Clone();
            foreach (var roleName in roleNames)
            {
                clone.ConfigSets = clone.ConfigSets.InsertOrUpdateNamedItem(roleName, r => r);
            }
            return clone;
        }

        public static VirtualMachine UsingDeploymentGroup(this VirtualMachine vm, string deploymentGroup)
        {
            if (deploymentGroup == null) throw new ArgumentNullException("deploymentGroup");

            var clone = vm.Clone();
            clone.DeploymentGroup = deploymentGroup;
            return clone;
        }

        public static BrewmasterTemplate WithVirtualNetwork(this BrewmasterTemplate template, VirtualNetworkSite network)
        {
            if (network == null) throw new ArgumentNullException("network");

            var clone = template.Clone();
            clone.Network.VirtualSites = clone.Network.VirtualSites.InsertOrUpdateNamedItem(network, v => v.Name);
            return clone;
        }

        public static BrewmasterTemplate WithVirtualNetwork(this BrewmasterTemplate template, string name,
                                                            Func<VirtualNetworkSite, VirtualNetworkSite> builder = null)
        {
            if (name == null) throw new ArgumentNullException("name");
            if (template.AffinityGroup == null)
                throw new InvalidOperationException("Must specify an affinity group first.");

            var vnet = new VirtualNetworkSite
                {
                    Name = name,
                    AffinityGroup = template.AffinityGroup.Name,
                };

            if (builder != null)
            {
                vnet = builder(vnet);
            }
            return WithVirtualNetwork(template, vnet);
        }

        public static VirtualNetworkSite WithAddressSpace(this VirtualNetworkSite vnet, params string[] addressPrefixes)
        {
            if (addressPrefixes == null) throw new ArgumentNullException("addressPrefixes");

            var clone = vnet.Clone();
            foreach (var prefix in addressPrefixes)
            {
                clone.AddressSpace = clone.AddressSpace.InsertOrUpdateNamedItem(prefix, s => s);
            }
            return clone;
        }

        public static VirtualNetworkSite WithSubnet(this VirtualNetworkSite vnet, string name, string addressPrefix)
        {
            if (name == null) throw new ArgumentNullException("name");

            var subnet = new Subnet
                {
                    Name = name,
                    AddressPrefix = addressPrefix
                };

            var clone = vnet.Clone();
            clone.Subnets = clone.Subnets.InsertOrUpdateNamedItem(subnet, s => s.Name);
            return clone;
        }

        public static VirtualNetworkSite UsingDnsServer(this VirtualNetworkSite vnet, params string[] dnsServers)
        {
            if (dnsServers == null) throw new ArgumentNullException("dnsServers");

            var clone = vnet.Clone();
            foreach (var server in dnsServers)
            {
                clone.DnsServers = clone.DnsServers.InsertOrUpdateNamedItem(server, s => s);
            }
            return clone;
        }

        public static VirtualNetworkSite WithSiteToSiteConnection(this VirtualNetworkSite vnet,
                                                                  string localNetworkSiteName)
        {
            if (localNetworkSiteName == null) throw new ArgumentNullException("localNetworkSiteName");

            var clone = vnet.Clone();
            clone.LocalSiteRefConnection = localNetworkSiteName;
            return clone;
        }

        public static VirtualNetworkSite WithPointToSiteConnection(this VirtualNetworkSite vnet,
                                                                   string vpnClientAddressPrefix)
        {
            if (vpnClientAddressPrefix == null) throw new ArgumentNullException("vpnClientAddressPrefix");

            var clone = vnet.Clone();
            clone.VPNClientAddressSpaces = 
                clone.VPNClientAddressSpaces.InsertOrUpdateNamedItem(vpnClientAddressPrefix, s => s);
            return clone;
        }

        public static BrewmasterTemplate WithLocalNetwork(this BrewmasterTemplate template,
                                                          string name, string vpnGatewayAddress,
                                                          params string[] addressSpaces)
        {
            if (name == null) throw new ArgumentNullException("name");

            var localNetwork = new LocalNetworkSite
                {
                    Name = name,
                    VPNGatewayAddress = vpnGatewayAddress,
                    AddressSpace = addressSpaces.CloneArray(),
                };

            return WithLocalNetwork(template, localNetwork);
        }

        public static BrewmasterTemplate WithLocalNetwork(this BrewmasterTemplate template, LocalNetworkSite network)
        {
            if (network == null) throw new ArgumentNullException("network");

            var clone = template.Clone();
            clone.Network.LocalSites = clone.Network.LocalSites.InsertOrUpdateNamedItem(network, v => v.Name);
            return clone;
        }

        public static BrewmasterTemplate WithDnsServer(this BrewmasterTemplate template, string name,
                                                       string ipAddress)
        {
            if (name == null) throw new ArgumentNullException("name");

            var server = new DnsServer
                {
                    Name = name,
                    IPAddress = ipAddress,
                };

            var clone = template.Clone();
            clone.Network.DnsServers = clone.Network.DnsServers.InsertOrUpdateNamedItem(server, s => s.Name);
            return clone;
        }
    }
}