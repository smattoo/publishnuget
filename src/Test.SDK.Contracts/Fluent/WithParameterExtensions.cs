using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using Brewmaster.TemplateSDK.Contracts.Models;
using Brewmaster.TemplateSDK.Contracts.Validation;
using JetBrains.Annotations;

namespace Brewmaster.TemplateSDK.Contracts.Fluent
{
    public static class WithParameterExtensions
    {
        public static BrewmasterTemplate WithParameter(this BrewmasterTemplate template, Parameter parameter)
        {
            if (parameter == null) throw new ArgumentNullException("parameter");

            var clone = template.Clone();
            clone.Parameters = clone.Parameters.InsertOrUpdateNamedItem(parameter, p => p.Name);
            return clone;
        }

        public static BrewmasterTemplate WithParameter(this BrewmasterTemplate template, string name, 
                                                       ParameterType type, string description, string typeHint,
                                                       Func<Parameter, Parameter> builder = null,
                                                       bool? isList = null, bool? maskValue = null)
        {
            if (name == null) throw new ArgumentNullException("name");
            if (String.IsNullOrWhiteSpace(description)) throw new ArgumentNullException("description");

            var parameter = new Parameter
                {
                    Type = type,
                    TypeHint = typeHint,
                    Name = name,
                    Description = description,
                    IsList = isList,
                    MaskValue = maskValue,
                };

            if (builder != null)
            {
                parameter = builder(parameter);
            }
            return WithParameter(template, parameter);
        }

        public static BrewmasterTemplate WithParameter(this BrewmasterTemplate template, string name,
                                                       ParameterType type, string description,
                                                       Func<Parameter, Parameter> builder = null,
                                                       bool? isList = null, bool? maskValue = null)
        {
            return WithParameter(template, name, type, description, null, builder, isList);
        }

        public static Parameter WithDefaultValue(this Parameter parameter, string value)
        {
            if (value == null) throw new ArgumentException("Null is not an acceptable default value.", "value");

            var clone = parameter.Clone();
            clone.Default = value;
            return clone;
        }

        public static Parameter WithRegexValidation(this Parameter parameter, string regex, string description = null)
        {
            if (String.IsNullOrWhiteSpace(regex)) throw new ArgumentNullException("regex");

            var clone = parameter.Clone();
            clone.AllowedRegex = regex;
            clone.AllowedDescription = description ?? clone.AllowedDescription;
            return clone;
        }

        public static Parameter WithAllowedValues(this Parameter parameter, string[] values, string description = null)
        {
            if (values == null || values.Length == 0) throw new ArgumentNullException("values");
            if (values.Any(s => s == null)) throw new ArgumentException("Allowed values cannot be null", "values");

            var clone = parameter.Clone();
            clone.AllowedValues = values;
            clone.AllowedDescription = description ?? clone.AllowedDescription;
            return clone;
        }

        public static Parameter WithLimits(this Parameter parameter, double? minimum, double? maximum)
        {
            if (parameter.Type == ParameterType.String)
            {
                if (minimum != (int?) minimum || maximum != (int?) maximum)
                {
                    throw new ArgumentException(
                        "Minimum and maximum must be whole numbers when applied to String parameters.");
                }
                if (minimum < Parameter.MinimumStringLength ||
                    minimum > (maximum ?? Parameter.MaximumStringLength))
                {
                    throw new ArgumentOutOfRangeException(
                        "minimum",
                        String.Format(
                            "Minimum must be a whole number between {0} and {1} when applied to String parameters.",
                            Parameter.MinimumStringLength, (int?) maximum ?? Parameter.MaximumStringLength));
                }
                if (maximum < (minimum ?? Parameter.MinimumStringLength) ||
                    maximum > Parameter.MaximumStringLength)
                {
                    throw new ArgumentOutOfRangeException(
                        "maximum",
                        String.Format(
                            "Maximum must be a whole number between {0} and {1} when applied to String parameters.",
                            (int?) minimum ?? Parameter.MinimumStringLength, Parameter.MaximumStringLength));
                }
            }
            else if (parameter.Type == ParameterType.Number)
            {
                if (minimum > maximum)
                {
                    throw new ArgumentOutOfRangeException(
                        "minimum", "Minimum value cannot be greater than the maximum value.");
                }
            }
            else
            {
                if (minimum.HasValue || maximum.HasValue)
                {
                    throw new NotSupportedException(
                        "Parameter's type does not support minimum or maximum value.");
                }
            }

            var clone = parameter.Clone();
            clone.Minimum = minimum;
            clone.Maximum = maximum;
            return clone;
        }

        public static string ProvideValues(this Parameter parameter)
        {
            string computedValue = string.Empty;
            
            switch (parameter.Type)
            {
                case ParameterType.String:
                    if (parameter.TypeHint == "AzureRegionName")
                    {
                        var randomizer = new Random();
                        var randomLen = randomizer.Next(3, ValidationHelpers.AzureRegions.Length - 1);

                        computedValue = ValidationHelpers.AzureRegions.ElementAt(randomLen);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(parameter.Default))
                        {
                            computedValue = parameter.Default;
                        }
                        else if (!string.IsNullOrEmpty(parameter.AllowedRegex))
                        {
                            var randomizer = new Random();
                            var randomLen = randomizer.Next(3, 14);
                            computedValue = StringExtensions.RandomAlphaString(randomLen);

                            //test it.
                            var reg = new Regex(parameter.AllowedRegex);
                            var isvalid = reg.IsMatch(computedValue);
                        }
                        else
                        {
                            var randomizer = new Random();
                            var min = parameter.Minimum.HasValue ? Convert.ToInt32(parameter.Minimum.Value) : 4;
                            var max = parameter.Maximum.HasValue ? Convert.ToInt32(parameter.Maximum.Value) : 10;
                            var randomLen = randomizer.Next(min, max);

                            if (parameter.Name == "AdminPassword")
                            {
                                computedValue = StringExtensions.RandomAlphanumString(randomLen, false);
                            }
                            else
                            {
                                computedValue = StringExtensions.RandomAlphaString(randomLen);
                            }
                            // computedValue.RandomAlphanumString()
                        }
                    }
                    break;
                case ParameterType.Number:
                    if (!string.IsNullOrEmpty(parameter.Default))
                    {
                        computedValue = parameter.Default;
                    }
                    else
                    {
                        if (parameter.Minimum != null)
                        {
                            var nextRandomNumber = new Random((int)(parameter.Minimum));// computedValue.RandomAlphanumString()
                            if (parameter.Maximum != null) 
                               computedValue = nextRandomNumber.Next((int)parameter.Maximum).ToString(CultureInfo.InvariantCulture);
                        }
                        //should we test it??
                        if (!string.IsNullOrEmpty(parameter.AllowedRegex))
                        {
                            var reg = new Regex(parameter.AllowedRegex);
                            var isvalid = reg.IsMatch(computedValue);
                        }
                    }
                    break;
                case ParameterType.Boolean:
                    break;
                case ParameterType.Date:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            Debug.WriteLine("Parameter: {0}, Type: {1}, Value: {2}", parameter.Name, parameter.TypeHint, computedValue);
            return computedValue;
        }

    }
}