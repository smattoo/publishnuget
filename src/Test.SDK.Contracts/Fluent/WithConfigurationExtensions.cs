﻿using Brewmaster.TemplateSDK.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Brewmaster.TemplateSDK.Contracts.Fluent
{
    public static class WithConfigurationExtensions
    {
        public static BrewmasterTemplate WithConfiguration(this BrewmasterTemplate template, Configuration config)
        {
            if (config == null) throw new ArgumentNullException("config");

            var clone = template.Clone();

            clone.Configurations = clone.Configurations.InsertOrUpdateNamedItem(config, c => c.Name);
            return clone;
        }

        public static BrewmasterTemplate WithConfiguration(this BrewmasterTemplate template, string name, string description,
                                                           Func<Configuration, Configuration> builder = null)
        {
            if (name == null) throw new ArgumentNullException("name");

            var config = new Configuration
                {
                    Name = name,
                    Description = description,
                };

            if (builder != null)
            {
                config = builder(config);
            }

            return template.WithConfiguration(config);
        }

        public static Configuration WithScript(this Configuration config, string name, Func<ScriptResource, ScriptResource> builder = null, string credential = null)
        {
            if (name == null) throw new ArgumentNullException("name");

            var script = new ScriptResource
                {
                    Name = name,
                    Credential = credential,
                };

            if (builder != null)
            {
                script = builder(script);
            }

            return config.WithResource(script);
        }

        public static Configuration WithResource(this Configuration config, GenericResource resource)
        {
            if (resource == null) throw new ArgumentNullException("resource");

            var clone = config.Clone();

            clone.Resources = clone.Resources.InsertOrUpdateNamedItem(resource, r => r.Name);
            return clone;
        }

        public static Configuration WithResource(this Configuration config, string type, string name, bool ensurePresent = true, string importModule = null, string importTypeName = null,
                                                 Func<GenericResource, GenericResource> builder = null)
        {
            if (name == null) throw new ArgumentNullException("name");
            if (type == null) throw new ArgumentNullException("type");

            var resource = new GenericResource(type)
                {
                    Name = name,
                    ImportModule = importModule,
                    ImportTypeName = importTypeName
                };

            resource.Args.Add("Ensure", ensurePresent ? "Present" : "Absent");

            if (builder != null)
            {
                resource = builder(resource);
            }

            return config.WithResource(resource);
        }

        public static Configuration WithResource(this Configuration config, string type, string name, bool ensurePresent = true,
                                                 Func<GenericResource, GenericResource> builder = null)
        {
            return WithResource(config, type, name, ensurePresent, null, null, builder);
        }

        public static GenericResource WithArgument(this GenericResource resource, string key, string value)
        {
            var clone = (GenericResource)resource.Clone();

            if (clone.Args.ContainsKey(key))
            {
                clone.Args.Remove(key);
            }

            clone.Args.Add(key, value);
            return clone;
        }

        public static T Requires<T>(this T resource, string type, string name) where T : GenericResource
        {
            var clone = (T)resource.Clone();

            var requirement = String.Format("[{0}]{1}", type, name);

            clone.Requires = clone.Requires.InsertOrUpdateNamedItem(requirement, s => s);
            return clone;
        }
    }
}
