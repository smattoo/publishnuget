using System.Collections.Generic;

namespace Brewmaster.TemplateSDK.Contracts
{
    public interface ITemplateSDKLiquidRenderer
    {
        string GenerateJson(string templateJson, IDictionary<string, string> templateArgs);
    }
}