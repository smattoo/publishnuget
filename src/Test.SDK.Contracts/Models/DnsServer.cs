namespace Brewmaster.TemplateSDK.Contracts.Models
{
    public class DnsServer
    {
        public string Name { get; set; }
        public string IPAddress { get; set; }
    }
}