using System;

namespace Brewmaster.TemplateSDK.Contracts.Models
{
    public class DeploymentGroup
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public static string DefaultName
        {
            get { return "default"; }
        }

        public static bool IsMatch(string name1, string name2)
        {
            name1 = String.IsNullOrEmpty(name1) ? DefaultName : name1;
            name2 = String.IsNullOrEmpty(name2) ? DefaultName : name2;
            return name1.EqualNoCase(name2);
        }
    }
}