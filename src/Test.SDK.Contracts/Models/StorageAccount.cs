namespace Brewmaster.TemplateSDK.Contracts.Models
{
    public class StorageAccount
    {
        public string Name { get; set; }
        public string AffinityGroup { get; set; }
        public string Region { get; set; }
        public string Description { get; set; }
        public string Label { get; set; }
        public bool DisableGeoReplication { get; set; }
    }
}