namespace Brewmaster.TemplateSDK.Contracts.Models
{
    public class Credential
    {
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}