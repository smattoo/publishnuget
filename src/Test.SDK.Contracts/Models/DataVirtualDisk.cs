using System;

namespace Brewmaster.TemplateSDK.Contracts.Models
{
    public class DataVirtualDisk
    {
        public string DiskId { get; set; }
        public string DiskName { get; set; }
        public Uri MediaLink { get; set; }
        public int? Lun { get; set; }
        public int? LogicalSizeInGB { get; set; }
        public string HostCaching { get; set; }
    }
}