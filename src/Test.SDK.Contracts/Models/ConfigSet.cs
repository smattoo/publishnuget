namespace Brewmaster.TemplateSDK.Contracts.Models
{
    public class ConfigSet
    {
        public ConfigSet()
        {
            Endpoints = new VmEndpoint[0];
            //CertificateIds = null;// new string[0];
            ConfigurationIds = new string[0];
        }
        public string Name { get; set; }
        public string Description { get; set; }
        public VmEndpoint[] Endpoints { get; set; }
        //public string[] CertificateIds { get; set; }
        public string[] ConfigurationIds { get; set; }
    }

}
