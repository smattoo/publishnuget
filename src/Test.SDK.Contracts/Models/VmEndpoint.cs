namespace Brewmaster.TemplateSDK.Contracts.Models
{
    public class VmEndpoint
    {
        public VmEndpoint()
        {
            Rules = new AccessControlRule[0];
            Protocol = "tcp";
            LoadBalancerProbe = null;
        }

        public string Name { get; set; }
        public int? LocalPort { get; set; }
        public string Protocol { get; set; }
        public int? Port { get; set; }
        public bool EnableDirectServerReturn { get; set; }
        public string VirtualIPAddress { get; set; }
        public AccessControlRule[] Rules { get; set; }
        public EndpointLoadBalancerProbe LoadBalancerProbe { get; set; }
    }
}