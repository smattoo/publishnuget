namespace Brewmaster.TemplateSDK.Contracts.Models
{
    public class Network
    {
        public Network()
        {
            DnsServers = new DnsServer[0];
            LocalSites = new LocalNetworkSite[0];
            VirtualSites = new VirtualNetworkSite[0];
        }

        public DnsServer[] DnsServers { get; set; }
        public LocalNetworkSite[] LocalSites { get; set; }
        public VirtualNetworkSite[] VirtualSites { get; set; }
    }
}