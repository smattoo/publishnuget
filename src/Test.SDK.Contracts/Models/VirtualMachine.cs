namespace Brewmaster.TemplateSDK.Contracts.Models
{
    public class VirtualMachine
    {
        public string Name { get; set; }
        public string RoleSize { get; set; }
        public string[] Subnets { get; set; }
        public string StaticVNetIPAddress { get; set; }
        public string AvailabilitySet { get; set; }
        public OsVirtualDisk OsVirtualDisk { get; set; }
        public DataVirtualDisk[] DataVirtualDisks { get; set; }
        public WindowsConfigSet WindowsConfigSet { get; set; }
        public VmEndpoint[] Endpoints { get; set; }
        public string[] ConfigSets { get; set; }
        public string DeploymentGroup { get; set; }
    }
}