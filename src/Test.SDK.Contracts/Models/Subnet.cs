namespace Brewmaster.TemplateSDK.Contracts.Models
{
    public class Subnet
    {
        public string Name { get; set; }
        public string AddressPrefix { get; set; }
    }
}