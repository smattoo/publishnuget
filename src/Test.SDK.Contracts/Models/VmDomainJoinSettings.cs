namespace Brewmaster.TemplateSDK.Contracts.Models
{
    public class VmDomainJoinSettings
    {
        public string DomainToJoin { get; set; }
        public string MachineOU { get; set; }
        public string CredentialId { get; set; }
    }
}