namespace Brewmaster.TemplateSDK.Contracts.Models
{
    public enum ParameterType
    {
        String,
        Number,
        Boolean,
        Date,
    }

    public class Parameter
    {
        /// <summary>
        /// The Name of the parameter. Required.
        /// <remarks>Must conform to the regex: "[a-zA-Z_][a-zA-Z0-9_]*"</remarks>
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The type of the parameter, either String or Number.
        /// </summary>
        public ParameterType Type { get; set; }

        /// <summary>
        /// The parameter subtype. Used to trigger special processing on the UI.
        /// </summary>
        /// <example>Random, Password, AzureStorageName, AzureCloudServiceName</example>
        public string TypeHint { get; set; }

        /// <summary>
        /// The description of the parameter. Required.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The default value of the parameter. Default is no default value.
        /// </summary>
        public string Default { get; set; }

        /// <summary>
        /// Indicates that the parameter should be interpreted as a list of comma seperated values. 
        /// Default is false (not a list).
        /// <remarks>Values should be quote-escaped. Values containing commas should be wrapped in quotes.
        /// <example>A value with ""quote"", "A value containing a comma (,)."</example>
        /// </remarks>
        /// </summary>
        public bool? IsList { get; set; }

        /// <summary>
        /// Indicates that the parameter value should be masked/hidden. Default is false.
        /// <remarks>Password and other sensitive parameters should set this property true.</remarks>
        /// </summary>
        public bool? MaskValue { get; set; }

        /// <summary>
        /// A list of the allowed values for the parameter. Default is no constraint.
        /// <remarks>Value comparison is case-insensitive. Use <see cref="AllowedRegex"/> 
        /// if case is important.</remarks>
        /// </summary>
        public string[] AllowedValues { get; set; }

        /// <summary>
        /// A Regular Expression describing the allowed values for the parameter. Default is no constraint.
        /// <remarks>If both <see cref="AllowedRegex"/> and <see cref="AllowedValues"/> are specified, 
        /// only the AllowedRegex is used for validation. The UI may show the AllowedValues in an editable 
        /// combo/list box.</remarks>
        /// </summary>
        public string AllowedRegex { get; set; }

        /// <summary>
        /// A friendly description of the allowed constraint(s). Default is the AllowedRegex or AllowedValues text.
        /// </summary>
        public string AllowedDescription { get; set; }

        /// <summary>
        /// The minimum value of a number parameter. Default is Double.MinValue.
        /// The minimum length of a string parameter. Must be a whole number. Default is Zero.
        /// </summary>
        public double? Minimum { get; set; }

        /// <summary>
        /// The maximum value of a number parameter. Default is Double.MaxValue.
        /// The maximum length of a string parameter. Must be a whole number. Default is 1024.
        /// </summary>
        public double? Maximum { get; set; }

        public static int MinimumStringLength = 0;
        public static int MaximumStringLength = 64*1024-1; //64K
    }
}