namespace Brewmaster.TemplateSDK.Contracts.Models
{
    public class EndpointLoadBalancerProbe
    {
        public string Name { get; set; }
        public int? Port { get; set; }
        public string Protocol { get; set; }
        public string Path { get; set; }
        public int? IntervalInSeconds { get; set; }
        public int? TimeoutInSeconds { get; set; }
    }
}