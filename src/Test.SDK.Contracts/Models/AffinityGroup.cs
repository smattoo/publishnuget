namespace Brewmaster.TemplateSDK.Contracts.Models
{
    public class AffinityGroup
    {
        public string Name { get; set; }
        public string Region { get; set; }
        public string Description { get; set; }
        public string Label { get; set; }
    }
}