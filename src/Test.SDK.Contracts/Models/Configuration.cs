using System.Collections.Generic;

namespace Brewmaster.TemplateSDK.Contracts.Models
{
    public class Configuration
    {
        public Configuration()
        {
            Resources = new GenericResource[0];
            Args = new Dictionary<string, string>();
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public GenericResource[] Resources { get; set; }
        public Dictionary<string, string> Args { get; set; }
    }
}