namespace Brewmaster.TemplateSDK.Contracts.Models
{
    public class AccessControlRule
    {
        public string Action { get; set; }
        public string Description { get; set; }
        public int Order { get; set; }
        public string RemoteSubnet { get; set; }
    }
}