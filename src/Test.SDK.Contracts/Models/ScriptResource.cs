namespace Brewmaster.TemplateSDK.Contracts.Models
{
    public class ScriptResource : GenericResource
    {
        public ScriptResource() 
            : base("Script")
        {
        }

        public override object Clone()
        {
            return new ScriptResource
                {
                    Credential = this.Credential,
                    GetScript = this.GetScript,
                    SetScript = this.SetScript,
                    TestScript = this.TestScript
                }.Copy(this);
        }

        public string Credential { get; set; }
        public string GetScript { get; set; }
        public string SetScript { get; set; }
        public string TestScript { get; set; }
    }
}