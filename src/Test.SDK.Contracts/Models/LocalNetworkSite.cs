namespace Brewmaster.TemplateSDK.Contracts.Models
{
    public class LocalNetworkSite
    {
        public LocalNetworkSite()
        {
            AddressSpace = new string[0];
        }
        public string Name { get; set; }
        public string VPNGatewayAddress { get; set; }
        public string[] AddressSpace { get; set; }
    }
}