namespace Brewmaster.TemplateSDK.Contracts.Models
{
    public class VmDeployment
    {
        public VmDeployment()
        {
            VirtualMachines = new VirtualMachine[0];
            DnsServers = null;
        }

        public string Name { get; set; }
        public string Label { get; set; }
        public string DeploymentId { get; set; }
        public string VirtualNetwork { get; set; }
        public VirtualMachine[] VirtualMachines { get; set; }
        public DnsServer[] DnsServers { get; set; }
        public string DiskStorageAccount { get; set; }
        public bool RemoveUnreferencedVms { get; set; }
    }
}