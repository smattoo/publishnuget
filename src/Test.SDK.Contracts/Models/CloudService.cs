namespace Brewmaster.TemplateSDK.Contracts.Models
{
    public class CloudService
    {
        public string Name { get; set; }
        public string AffinityGroup { get; set; }
        public string Region { get; set; }
        public string Label { get; set; }
        public string Description { get; set; }
        public VmDeployment Deployment { get; set; }
    }
}