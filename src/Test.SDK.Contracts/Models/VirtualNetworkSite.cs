namespace Brewmaster.TemplateSDK.Contracts.Models
{
    public class VirtualNetworkSite
    {
        public VirtualNetworkSite()
        {
            AddressSpace = new string[0];
            Subnets = new Subnet[0];
            DnsServers = new string[0];
            VPNClientAddressSpaces = new string[0];
        }

        public string Name { get; set; }
        public string AffinityGroup { get; set; }
        public string[] AddressSpace { get; set; }
        public Subnet[] Subnets { get; set; }
        public string[] DnsServers { get; set; }
        public string[] VPNClientAddressSpaces { get; set; }
        public string LocalSiteRefConnection { get; set; }
    }
}