using System;
using System.Collections.Generic;

namespace Brewmaster.TemplateSDK.Contracts.Models
{
    public class GenericResource : ICloneable
    {
        protected GenericResource()
        {
            Args = new Dictionary<string, string>();
            Nodes = new string[0];
            Requires = new string[0];
        }

        public GenericResource(string type) 
            : this()
        {
            this.Type = type;
        }

        public GenericResource(string type, string moduleName, string importType = null)
            : this(type)
        {
            this.ImportModule = moduleName;
            this.ImportTypeName = importType ?? Type;
        }

        public virtual object Clone()
        {
            return new GenericResource().Copy(this);
        }

        protected virtual GenericResource Copy(GenericResource src)
        {
            this.Type = src.Type;
            this.ImportModule = src.ImportModule;
            this.Name = src.Name;
            this.Args = src.Args.Clone();
            this.Nodes = src.Nodes.CloneArray();
            this.Requires = src.Requires.CloneArray();
            this.Description = src.Description;
            this.ImportTypeName = src.ImportTypeName;
            return this;
        }

        public string Type { get; set; }
        public string ImportModule { get; set; }
        public string ImportTypeName { get; set; }
        public string Name { get; set; }
        public Dictionary<string, string> Args { get; set; }
        public string[] Nodes { get; set; }
        public string[] Requires { get; set; }
        public string Description { get; set; }
    }
}