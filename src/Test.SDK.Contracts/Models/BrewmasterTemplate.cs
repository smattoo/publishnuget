namespace Brewmaster.TemplateSDK.Contracts.Models
{
    public class BrewmasterTemplate
    {
        public BrewmasterTemplate()
        {
            Parameters = new Parameter[0];

            Network = new Network();
            AffinityGroup = new AffinityGroup();
            StorageAccounts = new StorageAccount[0];
            CloudServices = new CloudService[0];

            DeploymentGroups = new DeploymentGroup[0];
            Credentials = new Credential[0];
            //Certificates = null; // new Certificate[0];
            ConfigSets = new ConfigSet[0];
            Configurations = new Configuration[0];
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public string Version { get; set; }

        public Parameter[] Parameters { get; set; }

        public Network Network { get; set; }
        public AffinityGroup AffinityGroup { get; set; }
        public StorageAccount[] StorageAccounts { get; set; }
        public CloudService[] CloudServices { get; set; }
        public DeploymentGroup[] DeploymentGroups { get; set; }

        public Credential[] Credentials { get; set; }
        //public Certificate[] Certificates { get; set; }
        public ConfigSet[] ConfigSets { get; set; }
        public Configuration[] Configurations { get; set; }

        public static BrewmasterTemplate Create(string name, string description)
        {
            return new BrewmasterTemplate
                {
                    Name = name,
                    Description = description
                };
        }
    }
}