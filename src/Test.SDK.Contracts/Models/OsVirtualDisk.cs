using System;

namespace Brewmaster.TemplateSDK.Contracts.Models
{
    public class OsVirtualDisk
    {
        public string DiskName { get; set; }
        public Uri MediaLink { get; set; }
        public string OsType { get; set; }
        public string OsImageName { get; set; }
    }
}