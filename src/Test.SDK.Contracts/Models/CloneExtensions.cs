using System;
using System.Collections.Generic;
using System.Linq;

namespace Brewmaster.TemplateSDK.Contracts.Models
{
    public static class CloneExtensions
    {
        /// <summary>
        /// Helper method used to perform a deep copy of an object.
        /// </summary>
        private static T CloneObj<T>(T obj)
        {
            if (typeof (T).IsPrimitive ||
                typeof (T) == typeof (String) ||
                typeof (T) == typeof (Guid) ||
                typeof (T) == typeof (DateTime) ||
                typeof (T) == typeof (TimeSpan) ||
                typeof (T) == typeof (Uri))
            {
                return obj; //immutable protocol
            }

            return (T) Clone((dynamic) obj);
        }

        /// <summary>
        /// Performs a deep copy of the array.
        /// </summary>
        /// <remarks>
        /// This is a replacement for <see cref="Array.Clone"/> which performs a shallow copy.
        /// </remarks>
        public static T[] CloneArray<T>(this T[] array)
        {
            return Clone(array);
        }

        /// <summary>
        /// Performs a deep copy of the array.
        /// </summary>
        /// <remarks>
        /// Note, this is not an extension method since Array implements IClonable (as a shallow copy).
        /// Use the <see cref="CloneArray{T}"/> extension method instead.
        /// </remarks>
        public static T[] Clone<T>( /*this*/ T[] array)
        {
            return array == null ? null : array.Select(CloneObj).ToArray();
        }

        /// <summary>
        /// Performs a deep copy of the list.
        /// </summary>
        public static List<T> Clone<T>(this List<T> list)
        {
            return list == null ? null : list.Select(CloneObj).ToList();
        }

        /// <summary>
        /// Performs a deep copy of the dictionary.
        /// </summary>
        /// <remarks>
        /// Note: The Dictionary key comparer is not cloned.
        /// </remarks>
        public static Dictionary<TKey, TValue> Clone<TKey, TValue>(this Dictionary<TKey, TValue> dictionary)
        {
            return dictionary == null
                       ? null
                       : dictionary.ToDictionary(pair => CloneObj(pair.Key),
                                                 pair => CloneObj(pair.Value),
                                                 dictionary.Comparer);
        }

        public static BrewmasterTemplate Clone(this BrewmasterTemplate template)
        {
            if (template == null)
                return null;

            return new BrewmasterTemplate
                {
                    Name = template.Name,
                    Description = template.Description,
                    Version = template.Version,

                    Parameters = template.Parameters.CloneArray(),
                    Network = template.Network.Clone(),
                    AffinityGroup = template.AffinityGroup.Clone(),
                    StorageAccounts = template.StorageAccounts.CloneArray(),
                    CloudServices = template.CloudServices.CloneArray(),

                    DeploymentGroups = template.DeploymentGroups.CloneArray(),
                    Credentials = template.Credentials.CloneArray(),
                    //Certificates = template.Certificates.CloneArray(),
                    ConfigSets = template.ConfigSets.CloneArray(),
                    Configurations = template.Configurations.CloneArray(),
                };
        }

        public static DeploymentGroup Clone(this DeploymentGroup deploymentGroup)
        {
            if (deploymentGroup == null)
                return null;

            return new DeploymentGroup
                {
                    Name = deploymentGroup.Name,
                    Description = deploymentGroup.Description,
                };
        }

        public static AffinityGroup Clone(this AffinityGroup affinityGroup)
        {
            if (affinityGroup == null)
                return null;

            return new AffinityGroup
                {
                    Name = affinityGroup.Name,
                    Description = affinityGroup.Description,
                    Region = affinityGroup.Region,
                    Label = affinityGroup.Label
                };
        }

        public static CloudService Clone(this CloudService cloudService)
        {
            if (cloudService == null)
                return null;

            return new CloudService
                {
                    Name = cloudService.Name,
                    AffinityGroup = cloudService.AffinityGroup,
                    Region = cloudService.Region,
                    Description = cloudService.Description,
                    Label = cloudService.Label,
                    Deployment = cloudService.Deployment.Clone(),
                };
        }

        public static StorageAccount Clone(this StorageAccount storage)
        {
            if (storage == null)
                return null;

            return new StorageAccount
                {
                    AffinityGroup = storage.AffinityGroup,
                    Region = storage.Region,
                    Name = storage.Name,
                    DisableGeoReplication = storage.DisableGeoReplication,
                    Description = storage.Description,
                    Label = storage.Label,
                };
        }

        public static Network Clone(this Network template)
        {
            if (template == null)
                return null;

            return new Network
                {
                    DnsServers = template.DnsServers.CloneArray(),
                    VirtualSites = template.VirtualSites.CloneArray(),
                    LocalSites = template.LocalSites.CloneArray(),
                };
        }

        public static DnsServer Clone(this DnsServer dnsServer)
        {
            if (dnsServer == null)
                return null;

            return new DnsServer
                {
                    Name = dnsServer.Name,
                    IPAddress = dnsServer.IPAddress
                };
        }

        public static LocalNetworkSite Clone(this LocalNetworkSite network)
        {
            if (network == null)
                return null;

            return new LocalNetworkSite
            {
                Name = network.Name,
                AddressSpace = network.AddressSpace.CloneArray(),
                VPNGatewayAddress = network.VPNGatewayAddress,
            };
        }

        public static VirtualNetworkSite Clone(this VirtualNetworkSite network)
        {
            if (network == null)
                return null;

            return new VirtualNetworkSite
                {
                    Name = network.Name,
                    AffinityGroup = network.AffinityGroup,
                    AddressSpace = network.AddressSpace.CloneArray(),
                    DnsServers = network.DnsServers.CloneArray(),
                    Subnets = network.Subnets.CloneArray(),
                    LocalSiteRefConnection = network.LocalSiteRefConnection,
                    VPNClientAddressSpaces = network.VPNClientAddressSpaces.CloneArray(),
                };
        }

        public static Subnet Clone(this Subnet subnet)
        {
            if (subnet == null)
                return null;

            return new Subnet
                {
                    Name = subnet.Name,
                    AddressPrefix = subnet.AddressPrefix
                };
        }

        public static VmDeployment Clone(this VmDeployment deploy)
        {
            if (deploy == null)
                return null;

            return new VmDeployment
                {
                    Label = deploy.Label,
                    DeploymentId = deploy.DeploymentId,
                    VirtualNetwork = deploy.VirtualNetwork,
                    Name = deploy.Name,
                    VirtualMachines = deploy.VirtualMachines.CloneArray(),
                    DnsServers = deploy.DnsServers.CloneArray(),
                    DiskStorageAccount = deploy.DiskStorageAccount,
                };
        }

        public static VirtualMachine Clone(this VirtualMachine machine)
        {
            if (machine == null)
                return null;

            return new VirtualMachine
                {
                    Name = machine.Name,
                    RoleSize = machine.RoleSize,
                    AvailabilitySet = machine.AvailabilitySet,
                    Subnets = machine.Subnets.CloneArray(),
                    DataVirtualDisks = machine.DataVirtualDisks.CloneArray(),
                    OsVirtualDisk = machine.OsVirtualDisk.Clone(),
                    WindowsConfigSet = machine.WindowsConfigSet.Clone(),
                    Endpoints = machine.Endpoints.CloneArray(),
                    ConfigSets = machine.ConfigSets.CloneArray(),
                    DeploymentGroup = machine.DeploymentGroup,
                };
        }

        public static VmEndpoint Clone(this VmEndpoint endpoint)
        {
            if (endpoint == null)
                return null;

            return new VmEndpoint
                {
                    LocalPort = endpoint.LocalPort,
                    Name = endpoint.Name,
                    Port = endpoint.Port,
                    Protocol = endpoint.Protocol,
                    EnableDirectServerReturn = endpoint.EnableDirectServerReturn,
                    VirtualIPAddress = endpoint.VirtualIPAddress,
                    Rules = endpoint.Rules.CloneArray(),
                    LoadBalancerProbe = endpoint.LoadBalancerProbe.Clone(),
                };
        }

        public static EndpointLoadBalancerProbe Clone(this EndpointLoadBalancerProbe probe)
        {
            if (probe == null)
                return null;

            return new EndpointLoadBalancerProbe
                {
                    Name = probe.Name,
                    Port = probe.Port,
                    IntervalInSeconds = probe.IntervalInSeconds,
                    Protocol = probe.Protocol,
                    Path = probe.Path,
                    TimeoutInSeconds = probe.TimeoutInSeconds,
                };
        }

        public static AccessControlRule Clone(this AccessControlRule rule)
        {
            if (rule == null)
                return null;

            return new AccessControlRule
                {
                    Action = rule.Action,
                    Description = rule.Description,
                    Order = rule.Order,
                    RemoteSubnet = rule.RemoteSubnet
                };
        }

        public static DataVirtualDisk Clone(this DataVirtualDisk disk)
        {
            if (disk == null)
                return null;

            return new DataVirtualDisk
                {
                    DiskName = disk.DiskName,
                    MediaLink = disk.MediaLink,
                    DiskId = disk.DiskId,
                    HostCaching = disk.HostCaching,
                    LogicalSizeInGB = disk.LogicalSizeInGB,
                    Lun = disk.Lun,
                };
        }

        public static OsVirtualDisk Clone(this OsVirtualDisk settings)
        {
            if (settings == null)
                return null;

            return new OsVirtualDisk
                {
                    DiskName = settings.DiskName,
                    MediaLink = settings.MediaLink,
                    OsImageName = settings.OsImageName,
                    OsType = settings.OsType,
                };
        }

        public static WindowsConfigSet Clone(this WindowsConfigSet configSet)
        {
            if (configSet == null)
                return null;

            return new WindowsConfigSet
                {
                    LocalAdminCredentialId = configSet.LocalAdminCredentialId,
                    ChangePasswordAtLogon = configSet.ChangePasswordAtLogon,
                    ComputerName = configSet.ComputerName,
                    DisableRdp = configSet.DisableRdp,
                    DomainJoinSettings = configSet.DomainJoinSettings.Clone(),
                    EnableAutomaticUpdates = configSet.EnableAutomaticUpdates
                };
        }

        public static VmDomainJoinSettings Clone(this VmDomainJoinSettings settings)
        {
            if (settings == null)
                return null;

            return new VmDomainJoinSettings
                {
                    DomainToJoin = settings.DomainToJoin,
                    CredentialId = settings.CredentialId,
                    MachineOU = settings.MachineOU
                };
        }

        public static Credential Clone(this Credential cred)
        {
            if (cred == null)
                return null;

            return new Credential
                {
                    Name = cred.Name,
                    UserName = cred.UserName,
                    Password = cred.Password
                };
        }

        //public static Certificate Clone(this Certificate cert)
        //{
        //    if (cert == null)
        //        return null;

        //    return new Certificate
        //        {
        //            Name = cert.Name,
        //            FileName = cert.FileName,
        //            Password = cert.Password
        //        };
        //}

        public static ConfigSet Clone(this ConfigSet configSet)
        {
            if (configSet == null)
                return null;

            return new ConfigSet
                {
                    Name = configSet.Name,
                    Description = configSet.Description,
                    Endpoints = configSet.Endpoints.CloneArray(),
                    //CertificateIds = configSet.CertificateIds.CloneArray(),
                    ConfigurationIds = configSet.ConfigurationIds.CloneArray(),
                };
        }

        public static Configuration Clone(this Configuration configuration)
        {
            if (configuration == null)
                return null;

            return new Configuration
                {
                    Name = configuration.Name,
                    Description = configuration.Description,
                    Args = configuration.Args.Clone(),
                    Resources = configuration.Resources.CloneArray()
                };
        }

        public static GenericResource Clone(this GenericResource res)
        {
            if (res == null)
                return null;

            return (GenericResource)res.Clone();
        }

        public static Parameter Clone(this Parameter p)
        {
            if (p == null)
                return null;

            return new Parameter
                {
                    Name = p.Name,
                    Description = p.Description,
                    Type = p.Type,
                    TypeHint = p.TypeHint,
                    IsList = p.IsList,
                    Default = p.Default,
                    MaskValue = p.MaskValue,
                    Minimum = p.Minimum,
                    Maximum = p.Maximum,
                    AllowedValues = p.AllowedValues.CloneArray(),
                    AllowedRegex = p.AllowedRegex,
                    AllowedDescription = p.AllowedDescription,
                };
        }
    }
}