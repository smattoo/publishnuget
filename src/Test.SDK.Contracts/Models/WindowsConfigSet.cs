namespace Brewmaster.TemplateSDK.Contracts.Models
{
    public class WindowsConfigSet
    {
        public string ComputerName { get; set; }
        public string LocalAdminCredentialId { get; set; }
        public bool EnableAutomaticUpdates { get; set; }
        public bool ChangePasswordAtLogon { get; set; }
        public bool DisableRdp { get; set; }
        public VmDomainJoinSettings DomainJoinSettings { get; set; }
    }
}