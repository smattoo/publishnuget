using System;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Sockets;

namespace Brewmaster.TemplateSDK.Contracts
{
    public class CIDRAddressSpace
    {
        public uint Prefix { get; private set; }
        public uint Mask { get; private set; }

        public CIDRAddressSpace(uint prefix, uint mask)
        {
            if ((prefix & ~mask) != 0)
                throw new ArgumentOutOfRangeException("prefix", "Prefix contains bits not in the mask");
            Prefix = prefix;
            Mask = mask;
        }

        public CIDRAddressSpace(int prefix, int mask)
            : this(unchecked((uint) prefix), unchecked((uint) mask))
        {
        }

        public CIDRAddressSpace(IPAddress prefixAddr, int prefixLength)
        {
            if (prefixAddr == null) throw new ArgumentNullException("prefixAddr");
            if (prefixLength < 0 || prefixLength > 32) throw new ArgumentOutOfRangeException("prefixLength");
            Prefix = ToUInt32(prefixAddr);
            Mask = ToMask(prefixLength);
            if ((Prefix & ~Mask) != 0)
                throw new ArgumentOutOfRangeException("prefixAddr", "Prefix contains bits not in the mask");
        }

        public CIDRAddressSpace(IPAddress prefixAddr, IPAddress maskAddr)
        {
            if (prefixAddr == null) throw new ArgumentNullException("prefixAddr");
            if (maskAddr == null) throw new ArgumentNullException("maskAddr");
            Prefix = ToUInt32(prefixAddr);
            Mask = ToUInt32(maskAddr);
            if ((Prefix & ~Mask) != 0)
                throw new ArgumentOutOfRangeException("prefixAddr", "Prefix contains bits not in the mask");
        }

        public static CIDRAddressSpace Parse(string cidrValue)
        {
            if (cidrValue == null) throw new ArgumentNullException("cidrValue");

            var parts = cidrValue.Split(new[] {'/'}, 2);
            if (parts.Length != 2)
                throw new ArgumentException("Invalid CIDR value.", "cidrValue");

            IPAddress prefixAddr;
            if (!IPAddress.TryParse(parts[0], out prefixAddr))
                throw new ArgumentException("Invalid routing prefix.", "cidrValue");

            int prefixLength;
            if (Int32.TryParse(parts[1], NumberStyles.None, CultureInfo.InvariantCulture, out prefixLength))
                return new CIDRAddressSpace(prefixAddr, prefixLength);

            IPAddress maskAddr;
            if (!IPAddress.TryParse(parts[1], out maskAddr))
                throw new ArgumentException("Invalid prefix mask.", "cidrValue");
            return new CIDRAddressSpace(prefixAddr, maskAddr);
        }

        public static bool TryParse(string cidrValue, out CIDRAddressSpace addressSpace)
        {
            try
            {
                addressSpace = Parse(cidrValue);
                return true;
            }
            catch
            {
                addressSpace = null;
                return false;
            }
        }

        public long MaxAddresses()
        {
            return ((long) ~Mask) + 1;
        }

        public bool Contains(IPAddress address)
        {
            var val = ToUInt32(address);
            return (val & this.Mask) == this.Prefix;
        }

        public bool Contains(CIDRAddressSpace other)
        {
            if ((other.Mask & this.Mask) != this.Mask)
                return false; //other is not a subset of this
            return (other.Prefix & this.Mask) == this.Prefix;
        }

        public bool Overlaps(CIDRAddressSpace other)
        {
            var minMask = this.Mask & other.Mask;
            return (this.Prefix & minMask) == (other.Prefix & minMask);
        }

        public override string ToString()
        {
            var prefixAddr = new IPAddress(BitConverter.GetBytes(Prefix).Reverse().ToArray());
            var prefixLength = GetPrefixLength(Mask);
            if (prefixLength != -1)
            {
                return prefixAddr + "/" + prefixLength.ToString(CultureInfo.InvariantCulture);
            }
            //Use the old way...
            var maskAddr = new IPAddress(BitConverter.GetBytes(Mask).Reverse().ToArray());
            return prefixAddr + "/" + maskAddr;
        }

        private static uint ToUInt32(IPAddress address)
        {
            if (address.AddressFamily == AddressFamily.InterNetworkV6)
            {
                if (!address.IsIPv4MappedToIPv6)
                {
                    throw new NotSupportedException("Only IPv4 addresses are currently supported.");
                }
                address = address.MapToIPv4();
            }
            var bytes = address.GetAddressBytes().Reverse().ToArray();
            return BitConverter.ToUInt32(bytes, 0);
        }

        private static uint ToMask(int prefixLength)
        {
            return unchecked((uint) (~0ul << (32 - prefixLength)));
        }

        private static int GetPrefixLength(uint mask)
        {
            switch (mask)
            {
                case 0x00000000u: return 0;
                case 0x80000000u: return 1;
                case 0xC0000000u: return 2;
                case 0xE0000000u: return 3;
                case 0xF0000000u: return 4;
                case 0xF8000000u: return 5;
                case 0xFC000000u: return 6;
                case 0xFE000000u: return 7;
                case 0xFF000000u: return 8;
                case 0xFF800000u: return 9;
                case 0xFFC00000u: return 10;
                case 0xFFE00000u: return 11;
                case 0xFFF00000u: return 12;
                case 0xFFF80000u: return 13;
                case 0xFFFC0000u: return 14;
                case 0xFFFE0000u: return 15;
                case 0xFFFF0000u: return 16;
                case 0xFFFF8000u: return 17;
                case 0xFFFFC000u: return 18;
                case 0xFFFFE000u: return 19;
                case 0xFFFFF000u: return 20;
                case 0xFFFFF800u: return 21;
                case 0xFFFFFC00u: return 22;
                case 0xFFFFFE00u: return 23;
                case 0xFFFFFF00u: return 24;
                case 0xFFFFFF80u: return 25;
                case 0xFFFFFFC0u: return 26;
                case 0xFFFFFFE0u: return 27;
                case 0xFFFFFFF0u: return 28;
                case 0xFFFFFFF8u: return 29;
                case 0xFFFFFFFCu: return 30;
                case 0xFFFFFFFEu: return 31;
                case 0xFFFFFFFFu: return 32;
                default: return -1; //unknown
            }
        }
    }
}