using System;
using System.Collections.Generic;
using System.Linq;
using DotLiquid;
using Newtonsoft.Json;

namespace Brewmaster.TemplateSDK.Contracts
{
    public class TemplateSDKLiquidRenderer : ITemplateSDKLiquidRenderer
    {
        public class JsonFilter
        {
            public static string Json(string text)
            {
                return JsonConvert.ToString(text, '"');
            }
        }

        static TemplateSDKLiquidRenderer()
        {
            Template.RegisterFilter(typeof(JsonFilter));
        }

        public Template ParseJson(string templateJson)
        {
            var temp = Template.Parse(templateJson);
            return temp;
        }

        public string GenerateJson(string templateJson, IDictionary<string, string> templateArgs)
        {
            var liquidTemplate = Template.Parse(templateJson);
            var liquidParameters = Hash.FromDictionary(templateArgs.ToDictionary(p => p.Key, p => (object) p.Value));
            //TODO: add any predefined variables here (dont overwrite declared vars)...

            var deploymentJson = liquidTemplate.Render(liquidParameters);
            return deploymentJson;
        }
    }
}